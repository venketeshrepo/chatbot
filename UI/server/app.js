/* eslint no-console: 0 */

'use strict';
// const cors = require('cors');
const express = require('express');
// const helmet = require('helmet');
const path = require('path');
// const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
//require for the migration to kick start, Can be later moved to audit calls file.
// const db = require('./database/db');
// const logger = require('./util/logger');
// const commonUtils = require('./util/commonUtil');

const app = express();
// view engine setup
app.engine('.html', require('ejs').__express);

app.disable('x-powered-by');
app.disable('X-Powered-By');
app.set('views',path.join(__dirname , '../dist/ffq-chat-ui/index'));
app.set('view engine', 'html');
var publicPath = path.resolve(__dirname, '../dist/ffq-chat-ui');
app.use(express.static(publicPath));
// app.use(helmet({
//     frameguard: false
// }));

// app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const isDeveloping = process.env.NODE_ENV !== 'production';
const port = isDeveloping ? 4000 : process.env.PORT;
var publicPath = path.resolve(__dirname, '../dist/ffq-chat-ui');
// var publicPath = path.resolve(__dirname, 'dist');
// app.use(express.static(publicPath));
// const env = process.env.API_URL;
const env = process.env.API_URL || 'http://localhost:3000';
app.get('/config', function (req, res) {
  res.send(process.env.API_URL);
});

app.post('/post', function (req, res) {
  //console.log('req.body.quote_number'+req.body.quote_number);
  res.render(path.join(publicPath,'index'), {
        lobs_quoted:req.body.lobs_quoted,
        quote_number:req.body.quote_number,
        first_name:req.body.first_name,
        last_name:req.body.last_name,
        street:req.body.street,
        apartment:req.body.apartment,
        city:req.body.city,
        state:req.body.state,
        zip_code:req.body.zip_code,
        gender:req.body.gender,
        environment_url:req.body.environment_url,
        page_name:req.body.page_name,
        test_email:req.body.test_email,
        buyOptions:req.body.buyOptions,
        selectedCrossSellLobs:req.body.selectedCrossSellLobs,
        isLiveChat:req.body.isLiveChat,
        appName:req.body.appName,
        name:req.body.name,
        agent_phone:req.body.agent_phone,
        user_type:req.body.user_type,
        inactive_time:req.body.inactive_time,
        residence_type:req.body.residence_type,
        parent_url:req.body.parent_url,
        agent_name:req.body.agent_name,
        orchUrl: process.env.API_URL 
    });
});

app.get('/chatbot', function (req, res) {
  //console.log('req.body.quote_number'+req.body.quote_number);
  res.render(path.join(publicPath,'index'), {
        lobs_quoted:req.body.lobs_quoted,
        quote_number:req.body.quote_number,
        first_name:req.body.first_name,
        last_name:req.body.last_name,
        street:req.body.street,
        apartment:req.body.apartment,
        city:req.body.city,
        state:req.body.state,
        zip_code:req.body.zip_code,
        gender:req.body.gender,
        environment_url:req.body.environment_url,
        page_name:req.body.page_name,
        test_email:req.body.test_email,
        buyOptions:req.body.buyOptions,
        selectedCrossSellLobs:req.body.selectedCrossSellLobs,
        isLiveChat:req.body.isLiveChat,
        appName:'refreshchatbot',
        name:req.body.name,
        agent_phone:req.body.agent_phone,
        user_type:req.body.user_type,
        inactive_time:req.body.inactive_time,
        residence_type:req.body.residence_type,
        parent_url:req.body.parent_url,
        agent_name:req.body.agent_name,
        orchUrl: process.env.API_URL 
        
        
    });
});

app.get('/livechat', function (req, res) {
  //console.log('req.body.quote_number'+req.body.quote_number);
  res.render(path.join(publicPath,'index'), {
        lobs_quoted:req.body.lobs_quoted,
        quote_number:req.body.quote_number,
        first_name:req.body.first_name,
        last_name:req.body.last_name,
        street:req.body.street,
        apartment:req.body.apartment,
        city:req.body.city,
        state:req.body.state,
        zip_code:req.body.zip_code,
        gender:req.body.gender,
        environment_url:req.body.environment_url,
        page_name:req.body.page_name,
        test_email:req.body.test_email,
        buyOptions:req.body.buyOptions,
        selectedCrossSellLobs:req.body.selectedCrossSellLobs,
        isLiveChat:req.body.isLiveChat,
        appName:'refreshlivechat',
        name:req.body.name,
        agent_phone:req.body.agent_phone,
        user_type:req.body.user_type,
        inactive_time:req.body.inactive_time,
        residence_type:req.body.residence_type,
        parent_url:req.body.parent_url,
        agent_name:req.body.agent_name,
        orchUrl: process.env.API_URL 
       
        
    });
});

app.post('/livechat', function (req, res) {
  //console.log('req.body.quote_number'+req.body.quote_number);
  res.render(path.join(publicPath,'index'), {
        lobs_quoted:req.body.lobs_quoted,
        quote_number:req.body.quote_number,
        first_name:req.body.first_name,
        last_name:req.body.last_name,
        street:req.body.street,
        apartment:req.body.apartment,
        city:req.body.city,
        state:req.body.state,
        zip_code:req.body.zip_code,
        gender:req.body.gender,
        environment_url:req.body.environment_url,
        page_name:req.body.page_name,
        test_email:req.body.test_email,
        buyOptions:req.body.buyOptions,
        selectedCrossSellLobs:req.body.selectedCrossSellLobs,
        isLiveChat:req.body.isLiveChat,
        appName:'refreshlivechat',
        name:req.body.name,
        agent_phone:req.body.agent_phone,
        user_type:req.body.user_type,
        inactive_time:req.body.inactive_time,
        residence_type:req.body.residence_type,
        parent_url:req.body.parent_url,
        agent_name:req.body.agent_name,
        orchUrl: process.env.API_URL 
       
        
    });
});

// app.post('/post', function (req, res) {
//   console.log(req.body);
//   // console.log(JSON.stringify(req.body));
//   var datetime = new Date();
//     console.log(datetime);
//   var urlObj = JSON.parse(JSON.stringify(req.body));
//   // let chtUrl = "/chatbot?"; 
//   let chtUrl = "/?"; 
//   let firstParam = true;

//   for (var key in urlObj) {


//     if (urlObj.hasOwnProperty(key)) {
//       if(firstParam){
//         chtUrl = chtUrl + key + "=" + urlObj[key];
//         firstParam = false;
//       }else{
//         chtUrl = chtUrl + "&" +  key + "=" + urlObj[key];
//         //chtUrl = app + "&" +  key + "=" + urlObj[key];
//       }
        
//     }
//   }

//   console.log("****UI "+chtUrl);
// res.redirect(chtUrl);
// // res.render(path.join(publicPath,'/'), {
// //        environment_url: 'www.google.com',
// //         quote_number: '1234'
// //     });
  
// });

app.all('/*', function(req, res, next) {
    // Just send the index.html for other files to support HTML5Mode
    var fileName = req.url;
    res.sendFile(fileName, { root: publicPath });
});

app.listen(port, function () {
  console.log('Server running on port X');
});