import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatbotHomeComponent } from './chatbot-home/chatbot-home.component';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { FfqChatMessagesComponent } from './chatbot-home/ffq-chat-messages/ffq-chat-messages.component';
import { FfqChatSenderComponent } from './chatbot-home/ffq-chat-sender/ffq-chat-sender.component';
import { AngularMaterialModule } from './shared/modules/angular-material/angular-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormatterPipe } from './shared/pipes/formatter.pipe';

const routes: Routes = [
		{ path: '', component: ChatbotHomeComponent }
];

@NgModule({
	declarations: [
		ChatbotHomeComponent,
		FfqChatMessagesComponent,
		FfqChatSenderComponent,
		FormatterPipe
		],
	imports: [
		CommonModule,
		FormsModule,
		RouterModule.forChild(routes),
		AngularMaterialModule,
		FlexLayoutModule
	]
})
export class FfqChatbotModule { }
