import { Component } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { ContextService } from './services/context.service';
import { IAppProperties } from './model/app-properties';

import { Store } from '@ngrx/store';
import * as appActions from '../store/app.actions';
import * as appSelectors from '../store/app.selectors';
import { AppData, AppContext } from './model/app.context';
import { AppActions, SetChatContext } from '../store/app.actions';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})

export class AppComponent {
	title = 'ffq-chat-ui';
	hasRouted = false;
	quoteNumber = 'quote_number' ;
	testUrl = 'testUrl';
	appName = 'appName';
	testEmail = 'test_email';
	agentName = 'agent_name';
	name = 'name';
	lobsQuoted = 'lobs_quoted';
	zipCode = 'zip_code';
	envUrl = 'environment_url';
	residenceType = 'residence_type';
	buyOptions = 'buyOptions';
	selectedCrossSellLob = 'selectedCrossSellLob';
	isLiveChat = 'isLiveChat';
	firstName = 'first_name';
	lastName = 'last_name';
	street = 'street';
	apartment = 'apartment';
	city = 'city';
	state = 'state';
	gender = 'gender';
	pageName = 'page_name';



	context: AppContext;
	context$ = this.store.select(appSelectors.getContext);
	constructor(private activatedRoute: ActivatedRoute, private router: Router, private store: Store<AppData>) {
		console.log('testUrl' + window[this.testUrl]);
		this.context$.subscribe(ctx => {this.context = ctx; });
		// const subscription = this.activatedRoute.queryParamMap.subscribe(queryParams => {
		if ((!this.hasRouted) && ( window[this.appName] )) {
				console.log('1 Inside AppComponent : ' +  window[this.appName]);
				// this.contextService.setAppName(queryParams.get('appName'));
				// tslint:disable-next-line: no-string-literal
				this.context.appName =  window[this.appName];
				// if (this.context.appName === 'chatbot'  || this.context.appName === 'refreshchatbot') {
				// 	console.log('about to updateChatbotContext');
				// 	if (this.context.appName === 'chatbot') {
				// 	this.context.chatBotContext = this.updateChatbotContext();
				// 	this.store.dispatch(new SetChatContext(this.context));
				// 	sessionStorage.setItem('context', JSON.stringify(this.context));
				// } else {
				// 	console.log('setting store with context from session storage chatbot');
				// 	this.store.dispatch(new SetChatContext(JSON.parse(sessionStorage.getItem('context'))));
				// }
				// 	// subscription.unsubscribe();
				// 	this.router.navigate(['/chatbot']);
				// } else if (this.context.appName === 'livechat' || this.context.appName === 'refreshlivechat') {
				// 	console.log('about to updateLivechatContext');
				// 	if (this.context.appName === 'livechat') {
				// 	this.context.liveChatContext =  this.updateLivechatContext();
				// 	this.store.dispatch(new SetChatContext(this.context));
				// 	sessionStorage.setItem('context', JSON.stringify(this.context));
				// } else {
				// 	console.log('setting store with context from session storage live chat');
				// 	this.store.dispatch(new SetChatContext(JSON.parse(sessionStorage.getItem('context'))));
				// 	console.log('context from session storage' + JSON.stringify(JSON.parse(sessionStorage.getItem('context'))));

				// }
				// 	// subscription.unsubscribe();
				// 	this.router.navigate(['/livechat']);
				// } else {
				console.log('Info Mocked############');
				this.context.chatBotContext =  this.updateChatbotContext();
				this.store.dispatch(new SetChatContext(this.context));
				sessionStorage.setItem('context', JSON.stringify(this.context));
				this.router.navigate(['/chatbot']);
				// }
				this.hasRouted = true;
			}
		// });
	}

	updateChatbotContext() {
		console.log('updateChatbotContext()');
		const appProperties: IAppProperties = {
			applicationCode: 'FFQUI',
			quote_number: window[this.quoteNumber],
			testUser: (window[this.testEmail] === 'true'),
			token: '',
			agent_name: window[this.agentName] || '',
			name: window[this.name] || 'User',
			application_code: 'FFQUI',
			agent_phone: '',

			user_type: '',
			lobs_quoted: window[this.lobsQuoted],
			zip_code: window[this.zipCode],
			environment_url: window[this.envUrl],
			residence_type: window[this.residenceType] || '',
			buyOptions: window[this.buyOptions],
			selectedCrossSellLobs: window[this.selectedCrossSellLob],
			isLiveChat: (window[this.isLiveChat] === 'Y'),
			userName: window[this.name] || '',
			userFName: '',
			userLName: '',
			street: '',
			apartment: '',
			city: '',
			state: '',
			gender: '',
			page_name: '',
			appName: ''

		};
		// this.contextService.setChatBotContext(appProperties);
		return appProperties;
	}

	updateLivechatContextMockLocal() {
		const appProperties: IAppProperties = {
			applicationCode: 'FFQUI',
			quote_number: '1529081971',
			testUser: false,
			token: '',
			agent_name: '',
			name: '',
			application_code: 'FFQUI',
			agent_phone: '',
			user_type: '',
			environment_url: '',
			residence_type: '',
			buyOptions: '',
			selectedCrossSellLobs: '',
			userName: '',
			isLiveChat: true,
			userFName: 'FirstNameMock',
			userLName: 'LastNameMock',
			street: 'StreetMock',
			apartment: 'ApartmentMock',
			city:  'CityMock',
			state: 'StateMock',
			gender: 'MFMOck',
			zip_code: 'zipMock',
			lobs_quoted: 'autoMocked',
			page_name: 'DRIVER',
			appName: 'livechatmock'




		};


		console.log('Customer First Name: ' + appProperties.userFName) ;
		console.log('Customer Last Name: ' + appProperties.userLName) ;
		console.log('Gender: ' + appProperties.gender) ;
		console.log('Apartment: ' + appProperties.apartment) ;
		console.log('Street: ' + appProperties.street) ;
		console.log('City: ' + appProperties.city) ;
		console.log('State: ' + appProperties.state) ;
		console.log('zip_code: ' + appProperties.zip_code) ;
		console.log('Page Name: ' + appProperties.page_name) ;

		// this.contextService.setChatBotContext(appProperties);
		return appProperties;
	}

	updateLivechatContext() {
		const appProperties: IAppProperties = {
			applicationCode: 'FFQUI',
			quote_number: window[this.quoteNumber],
			testUser: (window[this.testEmail] === 'true'),
			token: '',
			agent_name: '',
			name: '',
			application_code: 'FFQUI',
			agent_phone: '',
			user_type: '',
			environment_url: window[this.envUrl],
			residence_type: '',
			buyOptions: '',
			selectedCrossSellLobs: '',
			userName: '',
			isLiveChat: (window[this.isLiveChat] === 'Y'),
			userFName: window[this.firstName],
			userLName: window[this.lastName],
			street: window[this.street],
			apartment: window[this.apartment],
			city:  window[this.city],
			state: window[this.state],
			gender: window[this.gender],
			zip_code: window[this.zipCode],
			lobs_quoted: window[this.lobsQuoted],
			page_name: window[this.pageName],
			appName: window[this.appName]




		};


		console.log('Customer First Name: ' + appProperties.userFName) ;
		console.log('Customer Last Name: ' + appProperties.userLName) ;
		console.log('Gender: ' + appProperties.gender) ;
		console.log('Apartment: ' + appProperties.apartment) ;
		console.log('Street: ' + appProperties.street) ;
		console.log('City: ' + appProperties.city) ;
		console.log('State: ' + appProperties.state) ;
		console.log('zip_code: ' + appProperties.zip_code) ;
		console.log('Page Name: ' + appProperties.page_name) ;

		// this.contextService.setChatBotContext(appProperties);
		return appProperties;
	}

}
