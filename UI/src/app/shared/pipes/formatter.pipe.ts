import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'formatter'
})
export class FormatterPipe implements PipeTransform {

	transform(message: any, args?: any) {
		let link;
		let label;
		let msg = message;
		const regex = /\{#U#(.+?)\#U#}/g;
		const regex2 = /\{#P#(.+?)\#P#}/g;
		const trademark = /Covid/g;
	 // tslint:disable-next-line: only-arrow-functions
	 msg = msg.replace(regex, function(contents) {
	contents = contents.substring(
	contents.indexOf('{#U#') + 4,
	contents.lastIndexOf('#U#}')
	);
	const linkItem = contents.split(',');
	link = linkItem[0];
	label = linkItem[1];
	const html = '<a href=\'' + link + '\' target=\'_parent\'>' + label + '</a>';
	message = message.replace(regex, html );
		});
		// tslint:disable-next-line: only-arrow-functions
		msg = msg.replace(regex2, function(contents) {
				const html1 = '&nbsp;<a href="tel:$1" target="_parent">$1</a>';
				message = message.replace(regex2, html1 );
		});
		
		

		if(message.indexOf('\\n')>=0){
			
			let messageSplits: Array<string> = message.split('\\n');
		 
			let formattedMsg = '';
			messageSplits.forEach((msgItems, i) => {
			   formattedMsg = formattedMsg + msgItems.trim() + ((i!=messageSplits.length-1)? '<br/>': ' '); 
			});
			message = formattedMsg;

		}
		

		message = this.formatLink(message);



		return message;
	}


	private formatLink(plainText): string{
        let replacedText;
        let replacePattern1;
        let replacePattern2;
        let replacePattern3;

        //URLs starting with http://, https://, or ftp://
        replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
        replacedText = plainText.replace(replacePattern1, '<br/><a href="$1" target="_blank"><div class="link-break">$1</div></a><br/>');

        //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
        replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
        replacedText = replacedText.replace(replacePattern2, '$1,<br/><a href="http://$2" target="_blank"><div class="link-break">$2</div></a><br/>');

        //Change email addresses to mailto:: links.
        replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
        replacedText = replacedText.replace(replacePattern3, '<br/><a href="mailto:$1"><div class="link-break">$1</div></a><br/>');

        return replacedText;
       }

}


/*






*/