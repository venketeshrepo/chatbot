import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatGridListModule, MatButtonModule, MatDividerModule, MatListModule,
					MatIconModule, MatLineModule, MatCardModule } from '@angular/material';
import { MatExpansionModule } from '@angular/material/expansion';

@NgModule({
	declarations: [],
	imports: [
		CommonModule,
		MatGridListModule,
		MatButtonModule,
		MatDividerModule,
		MatListModule,
		MatIconModule,
		MatLineModule,
		MatCardModule,
		MatExpansionModule
	],
	exports: [
		MatGridListModule,
		MatButtonModule,
		MatButtonModule,
		MatDividerModule,
		MatListModule,
		MatIconModule,
		MatLineModule,
		MatCardModule,
		MatExpansionModule
	]
})
export class AngularMaterialModule { }
