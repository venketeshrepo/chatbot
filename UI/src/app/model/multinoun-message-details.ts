export class IMultinounMessageData {
	constructor() {}
	nouns: INounDetails[];
	}

export class INounDetails {
		constructor() {}
		prefix = 'Chasitor';
		noun = 'ChatMessage';
		object: IObjectDetails;
}

export class IObjectDetails {
		constructor() {}
		position ?: number;
		text: string;
}
