export class IMessageData {
	constructor() { }
	userName: string;
	message: string;
	timeStamp: string;
	messageType: string;

}
