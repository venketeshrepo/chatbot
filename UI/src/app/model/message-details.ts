export class IMessageDetails {
	constructor() { }
	message: string;
	messageType: string;
	userType: string;
	timeStamp: string;
	error: boolean;
	userName: string;
}
