export interface IAuthResponse {
	isBusinessHours: boolean;
	isGenericError: boolean;
	isServiceError: boolean;
	serviceErrorSorryMessage: string;
	serviceErrorTimeMessage: string;
	serviceErrorSuggessionMessage: string;
	message: string;
	preChatErrorStatus: boolean;
	preChatFailureMessage: string;
	buttonId: string;
	organizationId: string;
	deploymentId: string;
	liveAgentDomain: string;

}
