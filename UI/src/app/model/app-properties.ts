export interface IAppProperties {
	applicationCode: string;
	quote_number: string;
	testUser: boolean;
	token: string;
	agent_name: string;
	name: string;
	application_code: string;
	agent_phone: string;
	user_type: string;
	lobs_quoted: string;
	zip_code: string;
	environment_url: string;
	residence_type: string;
	buyOptions: string;
	selectedCrossSellLobs: string;
	isLiveChat: boolean;
	userName: string;
	userFName: string;
	userLName: string;
	street: string;
	apartment: string;
	city: string;
	state: string;
	gender: string;
	page_name: string;
	appName: string;

}
