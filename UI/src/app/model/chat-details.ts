import { IMessageDetails } from './message-details';
import { IAppProperties } from './app-properties';
export class IChatDetails {
		constructor() {}
		messageDetails: IMessageDetails;
		chatSessionId: string;
		// serviceName: string;
		// intentDetails: string;
		 chatForceDetails: ChatForceData;
		// session: ChatSession;
		appParam: IAppProperties;
		// isTransferedToSf: boolean;
}

export class ChatForceData {
	constructor() {}
	id: string;
	sequence: number ;
	affinityToken: string;
	key: string;
	multiNounCount: number;
	isCsrAcceptRequest: boolean;
	buttonId: string;
	connectSales: boolean;
	sfSessionId: string;
}

/*export class AuditData {
    chatSessionId : string;
    intentName : string;
    message: string;
}
export class ChatSession {
    constructor() {}
    transferAgent: string;
}*/

