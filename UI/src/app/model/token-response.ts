export interface ITokenResponse {
	message: string;
	token: string;
}
