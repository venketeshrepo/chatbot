import { IAppProperties } from './app-properties';
import { IMessageDetails } from './message-details';
export interface AppData {
		context: AppContext;
		messages: IMessageDetails[];
}

export interface AppContext {
		chatBotContext: IAppProperties;
		liveChatContext: IAppProperties;
		appName: string;
}
