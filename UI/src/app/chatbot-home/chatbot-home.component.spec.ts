// import { async, ComponentFixture, TestBed } from '@angular/core/testing';

// import { ChatbotHomeComponent } from './chatbot-home.component';
// import { FfqChatSenderComponent } from './ffq-chat-sender/ffq-chat-sender.component';
// import { FfqChatMessagesComponent } from './ffq-chat-messages/ffq-chat-messages.component';
// import { FfqChatDisclaimerComponent } from './ffq-chat-disclaimer/ffq-chat-disclaimer.component';
// import { AngularMaterialModule } from '../shared/modules/angular-material/angular-material.module';
// import { FormsModule } from '@angular/forms';
// import { RouterTestingModule } from '@angular/router/testing';
// import { HttpClientModule } from '@angular/common/http';
// import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
// import { FormatterPipe } from '../shared/pipes/formatter.pipe';
// import { StoreModule, Store } from '@ngrx/store';
// import { reducer } from '../../store/app.reducer';
// import * as ngrx from '@ngrx/store';
// import { of } from 'rxjs';
// import { AppData, AppContext } from '../model/app.context';

// fdescribe('ChatbotHomeComponent', () => {
// 	let component: ChatbotHomeComponent;
// 	let fixture: ComponentFixture<ChatbotHomeComponent>;
// 	// let store: Store<AppData>;
// 	const testStore = jasmine.createSpyObj('Store', ['select']);
// // 		const testappSelectors = jasmine.createSpyObj('appSelectors', ['getContext']);
// 	const initialState: AppData = {

// 		context: {
// 			chatBotContext: {
// 				applicationCode: '',
// 				quote_number: '',
// 				testUser: false,
// 				token: '',
// 				agent_name: '',
// 				name: '',
// 				application_code: '',
// 				agent_phone: '',
// 				user_type: '',
// 				lobs_quoted: '',
// 				zip_code: '',
// 				environment_url: '',
// 				residence_type: '',
// 				buyOptions: '',
// 				selectedCrossSellLobs: '',
// 				isLiveChat: false,
// 				userName: '',
// 				userFName: '',
// 				userLName: '',
// 				street: '',
// 				apartment: '',
// 				city: '',
// 				state: '',
// 				gender: '',
// 				page_name: '',
// 				appName: '',
// 			},
// 			liveChatContext: {
// 				applicationCode: '',
// 				quote_number: '',
// 				testUser: false,
// 				token: '',
// 				agent_name: '',
// 				name: '',
// 				application_code: '',
// 				agent_phone: '',
// 				user_type: '',
// 				lobs_quoted: '',
// 				zip_code: '',
// 				environment_url: '',
// 				residence_type: '',
// 				buyOptions: '',
// 				selectedCrossSellLobs: '',
// 				isLiveChat: false,
// 				userName: '',
// 				userFName: '',
// 				userLName: '',
// 				street: '',
// 				apartment: '',
// 				city: '',
// 				state: '',
// 				gender: '',
// 				page_name: '',
// 				appName: '',
// 			},
// 			appName: ''
// 		},
// 		messages: []
// 	};

// 	beforeEach(async(() => {

// 		TestBed.configureTestingModule({
// 			declarations: [ ChatbotHomeComponent
// 			, FfqChatSenderComponent
// 			, FfqChatMessagesComponent
// 			, FfqChatDisclaimerComponent, FormatterPipe ],
// 			imports: [AngularMaterialModule
// 			, FormsModule
// 			, RouterTestingModule
// 			, HttpClientModule
// 			, BrowserAnimationsModule// , StoreModule.forRoot({ appData: reducer })
// 			],
// 			 providers: [{ provide: Store, useValue: testStore }]
// 		})
// 		.compileComponents();
// 	}));

// 	beforeEach(() => {
// 		// spyOnProperty(ngrx, 'select').and.returnValue(of(initialState));
// 		fixture = TestBed.createComponent(ChatbotHomeComponent);
// 		component = fixture.componentInstance;

// 	// 	var testcontext = testappSelectors.getContext.and.returnValue(
// 		//   of(initialState.context)
// 		// );
// 	// var appSelectors ={getContext:{'test':'abs'}};
// 	// 	 testStore.select.and.returnValue(
// 		//   of(appSelectors.getContext)
// 		// );

// 		fixture.detectChanges();
// 	});

// 	fit('should create', () => {

// 		expect(component).toBeTruthy();
// 	});
// });
