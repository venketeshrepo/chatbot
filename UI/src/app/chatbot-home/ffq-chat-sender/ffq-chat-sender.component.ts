import { Component, OnInit } from '@angular/core';
import { IMessageDetails } from '../../model/message-details';
import { ChatSocketService } from '../../services/chat-socket.service';
import { MessageService } from '../../services/message.service';
import { SharedService } from '../../services/shared.service';
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-ffq-chat-sender',
	templateUrl: './ffq-chat-sender.component.html',
	styleUrls: ['./ffq-chat-sender.component.css']
})
export class FfqChatSenderComponent implements OnInit {
	userMessage: string;
	messages: IMessageDetails[] = [];
	outgoingMsg: IMessageDetails;
	enableSendButton = false;
	subscription: Subscription;

	constructor(private chatService: ChatSocketService, private msgService: MessageService, private sharedService: SharedService) {
		this.subscription = this.msgService.getMessage().subscribe(messageList => {
			this.messages = messageList;
		});
	}

	ngOnInit() {
	}

	onKeyUp($event) {

		if ($event.keyCode === 13) {
			this.sendMessage();


		} else {
			if (this.userMessage && this.userMessage.trim().length > 0) {
				this.enableSendButton = true;
			} else {
				this.enableSendButton = false;
			}
		}
	}

	onKeyDown($event) {

	}

	sendMessage() {
		if (this.userMessage.trim().length > 0) {
			this.outgoingMsg = {
				message: this.userMessage,
				messageType: 'message',
				userType: 'user',
				timeStamp: this.getDatetime(),
				error: false,
				userName: 'Visitor'
			};
			this.msgService.sendMessage(this.outgoingMsg);
			this.sharedService.setSendMessage(this.outgoingMsg);
			this.chatService.sendMessage(this.userMessage, false);
		}
		this.userMessage = '';
		this.enableSendButton = false;
	}

	getDatetime() {
		const date = new Date();
		let hours: any = date.getHours();
		let minutes: any = date.getMinutes();
		const ampm = hours >= 12 ? 'PM' : 'AM';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? '0' + minutes : minutes;
		hours = hours < 10 ? '0' + hours : hours;
		const strTime = hours + ':' + minutes + '' + ampm;
		return strTime;
	}

}
