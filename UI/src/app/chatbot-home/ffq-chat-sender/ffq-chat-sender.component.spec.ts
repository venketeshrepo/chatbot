import { async, ComponentFixture, TestBed } from '@angular/core/testing';


import { FfqChatSenderComponent } from './ffq-chat-sender.component';
import { AngularMaterialModule } from '../../shared/modules/angular-material/angular-material.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { reducer } from '../../../store/app.reducer';

fdescribe('FfqChatSenderComponent', () => {
	let component: FfqChatSenderComponent;
	let fixture: ComponentFixture<FfqChatSenderComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ FfqChatSenderComponent ],
			imports : [AngularMaterialModule, FormsModule
			, HttpClientModule , StoreModule.forRoot({ appData: reducer })],
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(FfqChatSenderComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	fit('should create#####', () => {
		expect(component).toBeTruthy();
	});

	fit('should call getDatetime', () => {
			component.getDatetime();
			expect(component).toBeTruthy();
	});
});
