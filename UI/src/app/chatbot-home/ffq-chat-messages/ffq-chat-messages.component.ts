import { Component, OnInit, AfterViewInit, AfterContentChecked } from '@angular/core';
import { MessageService } from '../../services/message.service';
import { SharedService } from '../../services/shared.service';
import { IMessageDetails } from '../../model/message-details';
import { Subscription } from 'rxjs';
import { ChangeDetectorRef } from '@angular/core';
import { ChatSocketService } from '../../services/chat-socket.service';

@Component({
	selector: 'app-ffq-chat-messages',
	templateUrl: './ffq-chat-messages.component.html',
	styleUrls: ['./ffq-chat-messages.component.css']
})
export class FfqChatMessagesComponent implements OnInit, AfterContentChecked {
	subscription: Subscription;
	subscription1: Subscription;
	subscription2: Subscription;
	messages: IMessageDetails[] = [];
	scrollHeight = 0;
	showLoad = true;
	isOrchConnectionFailure = false;
	connectionFailureMsg: any;
	showSuggestions = true;
	suggestionMessage: IMessageDetails;
	showChatTyping = false;
	hideSuggestion = []
	activeTimerBotThinking: any;

	constructor(private chatService: ChatSocketService, private msgService: MessageService, private ref: ChangeDetectorRef,
		private sharedService: SharedService) {
		this.subscription = this.msgService.getMessage().subscribe(messageList => {
			this.showLoad = false;
			console.log('Message List', messageList);

			this.messages = messageList;
			// this.addMessageToDisplayWindow(messageList);
		});

		this.subscription1 = this.sharedService.getSendMessage().subscribe(getMessage => {
			console.log('user send message : ', getMessage);
			this.showChatTyping = true;

			this.sharedService.playSendMsgAudio();
			this.activeTimerBotThinking = setTimeout(() => {

				if(this.messages[this.messages.length-1].userType=="user" && this.showChatTyping==true){

					this.showChatTyping = false;
					console.log("Rephrase question ?");
	
					
					let dateTimeStr = sharedService.getDatetime();
	
					this.sharedService.playIncomingMsgAudio();
					 this.messages.push({
						error: false,
						message: "Can you rephrase your question ?",
						messageType: "message",
						timeStamp: dateTimeStr,
						userType: "bot",
						userName: ''
					})					
				}

				
			}, 16000);

			
			console.log("TImer created: activeTimerBotThinking" + this.activeTimerBotThinking);

			this.showSuggestions = false;
		});

		this.subscription2 = this.sharedService.getLastRecievedMessage().subscribe(getLastReceivedMsg => {
			let msgReceived: string = getLastReceivedMsg;



			console.log("Clearing this.activeTimerBotThinking" + this.activeTimerBotThinking);
			clearTimeout(this.activeTimerBotThinking);

			console.log('Last recieved message from BOT is ', getLastReceivedMsg);
			
			if(msgReceived.indexOf('_BOT_PROGRESS')>0){
				this.showChatTyping = false;

				setTimeout(() => {
					this.showChatTyping = true;
				}, 800);
			}else{
				this.showChatTyping = false;
				
				setTimeout(() => {
					this.showSuggestions = true;
				}, 2000);

				
			}

			
		});
	}
	ngAfterContentChecked(): void {
		
		for (let index = 0; index < this.messages.length; index++) {
			this.hideSuggestion[index] = true;
		}
		this.hideSuggestion[this.messages.length-1] = false;
	//  console.debug("this.messages: ", this.messages);
	}

	ngAfterViewInit(): void {
	//	debugger
	//  console.debug("this.messages: ", this.messages);
	}

	formatBotMsg(msgStr: string) {

		
		return msgStr.replace('\\n', '<br/>');
	}

	ngOnInit() {
	}

	sendMessage(isSuggestionMsg, sugMessage, msgIndex) {
		this.showChatTyping = true;
		this.hideSuggestion[msgIndex] = true;
		if (sugMessage.trim().length > 0) {
			this.showSuggestions = isSuggestionMsg;
			this.suggestionMessage = {
				message: sugMessage,
				messageType: 'message',
				userType: 'user',
				timeStamp: this.getDatetime(),
				error: false,
				userName: 'Visitor'
			};
			this.msgService.sendMessage(this.suggestionMessage);
			this.sharedService.setSendMessage(this.suggestionMessage);
			this.chatService.sendMessage(sugMessage, false);
		}
		sugMessage = '';
	}

	getDatetime() {
		const date = new Date();
		let hours: any = date.getHours();
		let minutes: any = date.getMinutes();
		const ampm = hours >= 12 ? 'PM' : 'AM';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? '0' + minutes : minutes;
		hours = hours < 10 ? '0' + hours : hours;
		const strTime = hours + ':' + minutes + '' + ampm;
		return strTime;
	}

	printt(dattaa ){
		
		console.log(dattaa);
	}
}
