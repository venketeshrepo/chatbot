import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FfqChatMessagesComponent } from './ffq-chat-messages.component';
import { AngularMaterialModule } from '../../shared/modules/angular-material/angular-material.module';
import { IMessageDetails } from '../../model/message-details';
import { FormatterPipe } from '../../shared/pipes/formatter.pipe';
import { StoreModule } from '@ngrx/store';
import { reducer } from '../../../store/app.reducer';

fdescribe('FfqChatMessagesComponent', () => {
	let component: FfqChatMessagesComponent;
	let fixture: ComponentFixture<FfqChatMessagesComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ FfqChatMessagesComponent
				, FormatterPipe],
				imports: [AngularMaterialModule , StoreModule.forRoot({ appData: reducer })]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(FfqChatMessagesComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	fit('should create', () => {
		expect(component).toBeTruthy();
	});

	fit('should call addMessageToDisplayWindow', () => {
		const messagedata: IMessageDetails[] = [{

	message: '',
	messageType: '',
	userType: '',
	timeStamp: '',
	error: true,
	userName: ''

}];
		// let messagedatarray = [];
	// messagedatarray.push(messagedata);
		component.addMessageToDisplayWindow(messagedata);
		expect(component).toBeTruthy();
	});
});
