import { Component, OnInit, Renderer } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IMessageDetails } from '../model/message-details';
import { ChatSocketService } from '../services/chat-socket.service';
import { MessageService } from '../services/message.service';
import { ContextService } from '../services/context.service';
import { Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';
import { IAppProperties } from '../model/app-properties';
import { ITokenResponse } from '../model/token-response';
import { ElementRef, ViewChild, AfterViewChecked } from '@angular/core';
import { Store, Action } from '@ngrx/store';
import { AppData, AppContext } from '../model/app.context';
import * as appSelectors from '../../store/app.selectors';
import { SharedService } from '../services/shared.service';


@Component({
	selector: 'app-chatbot-home',
	templateUrl: './chatbot-home.component.html',
	styleUrls: ['./chatbot-home.component.css']
})
export class ChatbotHomeComponent  implements  OnInit, AfterViewChecked {
	@ViewChild('chatbotContainer', {static: false}) private elementView: ElementRef;
	@ViewChild('chatMsgDisplayArea', {static: false}) private chatMsgDisplayArea: ElementRef;

	left = true;
	toggleArrow = 'right-open';
	showpanel = true;
	online = true;
	disableTextBox = false;
	messages: IMessageDetails[] = [];
	offlineMessage = 'You seem to be offline. Kindly check your internet connection.';
	messagebox = '';
	subscription: Subscription;
	isChrome = false;
	viewHeight: number;
	chatMsgAreaHeight: number;
	scrollHeight = 0;
	context: AppContext;
	context$ = this.store.select(appSelectors.getContext);
	chatbotMsgAreaWidth: number;
	messageWidth: number;
	disclaimerWidth: number;

	constructor(private route: ActivatedRoute, private chatService: ChatSocketService
	,           private msgService: MessageService, private store: Store<AppData>
	,		         private sharedService: SharedService, private renderer: Renderer, private el: ElementRef) {
		console.log('chatbot-home-constructor');
		this.context$.subscribe(ctx => {
			this.context = ((ctx.liveChatContext === null) ? JSON.parse(sessionStorage.getItem('context')) : ctx);
	});
		const appProperties: IAppProperties = this.context.chatBotContext;
		// detect browser for sound IE issue fix
		// tslint:disable-next-line: no-string-literal
		this.isChrome = !!window['chrome'] && !!window['chrome'].webstore;
		if (appProperties !== undefined) {
			this.chatService.initSocketConnection(appProperties);
		}
	}


	sendMessage() {

		if (this.messagebox.length > 0) {
			this.messages.push({
				message: this.messagebox,
				messageType: 'message',
				userType: 'user',
				timeStamp: this.getDatetime(),
				error: false,
				userName: 'Visitor'
			});
			this.chatService.sendMessage(this.messagebox, false);
			this.messagebox = '';
		}
	}

	onKeyUp($event) {

		if ($event.keyCode === 13) {
			this.sendMessage();
		}
	}


	getDatetime() {
		const date = new Date();
		let hours: any = date.getHours();
		let minutes: any = date.getMinutes();
		const ampm = hours >= 12 ? 'PM' : 'AM';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? '0' + minutes : minutes;
		hours = hours < 10 ? '0' + hours : hours;
		const strTime = hours + ':' + minutes + '' + ampm;
		return strTime;
	}

	ngAfterViewChecked() {
		this.viewHeight = this.elementView.nativeElement.offsetHeight;
		this.sharedService.setContainerHeight(this.viewHeight);

		this.chatbotMsgAreaWidth =  this.chatMsgDisplayArea.nativeElement.offsetWidth;
		this.messageWidth = this.chatbotMsgAreaWidth - ((37 / 100) * this.chatbotMsgAreaWidth);

		const ua = navigator.userAgent;
		// IE & Edge browser specific changes start here - Incoming outgoing messages word wrapping issue fix
		if (ua.indexOf('MSIE ') > 0 || ua.indexOf('Trident') > 0 || ua.indexOf('Edge') > 0) {
			console.log('IE browser detected !!!!!');
			// tslint:disable-next-line: prefer-const
			let outgoingMsgs = this.el.nativeElement.querySelectorAll('.outgoing-message');
			Array.from(outgoingMsgs).forEach(part1 => {
				if (part1 !== null && part1 !== undefined) {
					this.renderer.setElementStyle(part1, 'word-wrap', 'break-word');
					this.renderer.setElementStyle(part1, 'max-width', (this.messageWidth + 'px'));
				}
			});

			// tslint:disable-next-line: prefer-const
			let incomingMsgs = this.el.nativeElement.querySelectorAll('.incoming-message');
			Array.from(incomingMsgs).forEach(part2 => {
				if (part2 !== null && part2 !== undefined) {
					this.renderer.setElementStyle(part2, 'word-wrap', 'break-word');
					this.renderer.setElementStyle(part2, 'max-width', (this.messageWidth + 'px'));
				}
			});
		}
		// IE & Edge browser specific changes end here

		// IE browser specific changes start here - Disclaimer text word wrapping issue fix
		this.disclaimerWidth = ((95.8 / 100) * this.chatbotMsgAreaWidth);
		if (ua.indexOf('MSIE ') > 0 || ua.indexOf('Trident') > 0 ) {
			const part = this.el.nativeElement.querySelector('.mat-expansion-panel-body');
			this.renderer.setElementStyle(part, 'max-width', (this.disclaimerWidth + 'px'));
			this.renderer.setElementStyle(part, 'word-wrap', 'break-word');
		}
		// IE browser specific changes end here
		if (this.scrollHeight < this.chatMsgDisplayArea.nativeElement.scrollHeight) {
			this.chatMsgDisplayArea.nativeElement.scrollTop = this.chatMsgDisplayArea.nativeElement.scrollHeight;
			this.scrollHeight = this.chatMsgDisplayArea.nativeElement.scrollHeight;
		}

	}

	ngOnInit() {


	}


}
