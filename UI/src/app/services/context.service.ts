import { Subject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { IAppProperties } from '../model/app-properties';


@Injectable({
providedIn: 'root'
})

export class ContextService {

private chatBotContext: IAppProperties;
private liveChatContext: IAppProperties;
private appName: string;

constructor() { }

getAppName() {
return this.appName;
}

setAppName(appName: string) {
this.appName = appName;
}

setChatBotContext(chatBotContext: IAppProperties) {
this.chatBotContext = chatBotContext;
}

getChatBotContext() {
return this.chatBotContext;
}

}
