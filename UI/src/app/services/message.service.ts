import { Subject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import { Store } from '@ngrx/store';
import { AppActions, SendMessage, RecieveMessage } from '../../store/app.actions';
import { IMessageDetails } from '../model/message-details';
import { AppData } from '../model/app.context';
import * as appActions from '../../store/app.actions';
import * as appSelectors from '../../store/app.selectors';


@Injectable({
		providedIn: 'root'
})

export class MessageService {

		messages: IMessageDetails[];
		allSuggestionMsgs: any = [];
		messages$ = this.store.select(appSelectors.getMessages);

		suggesionsSent = [];

		constructor(private store: Store<AppData>) {
			this.messages$.subscribe(messages => this.messages = messages);
		}

		public setAllSuggestionMsgs(allMsgs) {
			
			this.allSuggestionMsgs = allMsgs;
		}

		public getAllSuggestionMsgs(){
			
			return this.allSuggestionMsgs;
		}

		updateMessage(msg: any) {
			// this.msgSubject.next(msg);
			this.messages.push(msg);
			this.store.dispatch(new RecieveMessage(this.messages));
		}


		sendMessage(msg: any) {
			this.messages.push(msg);
			this.store.dispatch(new SendMessage(this.messages));
		}


		getMessage(): Observable<any> {
				// return this.msgSubject.asObservable();
				return this.messages$;
		}

		getRandomNumber(start: number, end: number){
			return Math.floor(Math.random() * end) + start  
		}
	
		getSuggesionMessage(suggestionsArray: Array<any>){

			
			let suggesionsToSent = [];
			let totalQuestions = suggestionsArray.length;
	
			if(this.suggesionsSent.length==10){
				this.suggesionsSent = [];
			}
	
			let loopCount = 0;
			let thisSessionSent = [];

			while (suggesionsToSent.length<3) {

				loopCount++;
				let randomNum = this.getRandomNumber(0, totalQuestions);
				

				// console.log("randomNum: " + randomNum);
				// console.debug("this.suggesionsSent: ",this.suggesionsSent);
				if(this.suggesionsSent.includes(randomNum)===false) {
					suggesionsToSent.push(suggestionsArray[randomNum]);
					this.suggesionsSent.push(randomNum);
					thisSessionSent.push(randomNum);

					if(this.suggesionsSent.length!=3 && this.suggesionsSent.length==suggestionsArray.length){
						this.suggesionsSent = thisSessionSent;

					}
				}


				if(loopCount>500)
				break;
			} 
	
			return suggesionsToSent;
	
		}
}
