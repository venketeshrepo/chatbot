import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { IAppProperties } from '../model/app-properties';
import { ITokenResponse } from '../model/token-response';
import { MessageService } from '../services/message.service';
import { IAuthResponse } from '../model/auth-response';
import { SharedService } from '../services/shared.service';
import { ChatForceData } from '../model/chat-details';
import { IMultinounMessageData, INounDetails, IObjectDetails } from '../model/multinoun-message-details';
import { IMessageDetails } from '../model/message-details';




@Injectable({
	providedIn: 'root'
})

export class ChatSocketService {

	private tokenURL;
	private socketUrl;
	private socket;
	private token;
	private userAttributes: IAppProperties;
	private chatPropertyData: any;
	private usermsgpushedflag: any; // to be implemented by redux
	private appParam: any; // to be implemented by redux
	private chatForceDetails: ChatForceData;
	private firstmessage: any;
	private nouns: any;
	isConnectionLost = false;
	private multiNounWrapper: INounDetails;
	private multiNounmessageObject: IObjectDetails;
	private multiNounRequestBody: IMultinounMessageData;
	chatRequest: any;
	orchUrl = 'orchUrl';
	sampleMsg: IMessageDetails;
	
	isDisconnected = false;
	disconnectStartTime;
	csrWaitingTimer;

	getusermsgpushedflag() {
		return this.usermsgpushedflag;
	}
	setusermsgpushedflag(flag) {
		this.usermsgpushedflag = flag;
	}

	getappParam() {
		return this.appParam;
	}
	setMessageForMultiNoun(sequence, message) {
		const multmessage = { sequence: 1, messageDetails: message };
		this.multiNounmessageObject = new IObjectDetails();
		this.multiNounmessageObject.position = sequence;
		this.multiNounmessageObject.text = message;
		this.multiNounWrapper = new INounDetails();
		this.multiNounWrapper.object = this.multiNounmessageObject;
		this.nouns.push(this.multiNounWrapper);
	}


	constructor(private http: HttpClient, private msgService: MessageService, private sharedService: SharedService) {
		this.loadConfig();
		this.usermsgpushedflag = false;
		this.appParam = {};
		this.chatForceDetails = new ChatForceData();
		this.nouns = [];
	}

	public connectToSocket(attributes: IAppProperties, messageService: MessageService, sharedService: SharedService) {
		console.log('Connect to socket!!!');
		this.socket = io(this.socketUrl, {
			reconnection: true,
			reconnectionDelay: 300000,
			reconnectionAttempts: Infinity,
			query: {
				quote_number: attributes.quote_number,
				name: attributes.name,
				agent_phone: attributes.agent_phone,
				agent_name: attributes.agent_name,
				user_type: attributes.user_type,
				lobs_quoted: attributes.lobs_quoted,
				zip_code: attributes.zip_code,
				environment_url: attributes.environment_url,
				residence_type: attributes.residence_type,
				token: this.token
			},
			secure: true,
			transports: ['websocket']
			// transports: ["polling"]
		});


		this.socket.on('keptAlive', (data) => {
			console.log('kept alive');
		});
		// on reconnection, reset the transports option, as the Websocket
		// connection may have failed (caused by proxy, firewall, browser, ...)
		this.socket.on('reconnect_attempt', () => {
			console.log('reconnect_attempt');
			console.log('updateAttribute');
			this.socket.emit('updateAttribute', this.userAttributes);
		});

		// Live Chat Welcome Message
		console.log('welcome message emit');
		this.socket.emit('welcome message', this.userAttributes);

		this.socket.on('LiveChatWelcomeMessageUpdateToScreen', function(data) {


			
			messageService.setAllSuggestionMsgs(data.message[2]);

			
			let allMessageSugestions:any = messageService.getAllSuggestionMsgs();
			
			this.showLoad = false;
			console.log('Data: \n', data);
			console.log('7: listening LiveChatWelcomeMessageUpdateToScreen(socket.on): ' + data.message.length);
			sessionStorage.setItem('sessionID', data.sessionID);
			sessionStorage.setItem('intentFulfilled', 'true');
			sharedService.playIncomingMsgAudio();
			messageService.updateMessage({
				message: data.message[0],
				messageType: 'welcomeMessage',
				userType: 'bot',
				timeStamp: sharedService.getDatetime(),
				error: false
			});
			setTimeout(function() {
				sharedService.playIncomingMsgAudio();
				messageService.updateMessage({
					message: data.message[1],
					messageType: 'welcomeMessage',
					userType: 'bot',
					timeStamp: sharedService.getDatetime(),
					error: false
				});

			}, 1 * 1000);
			setTimeout(function () {

				
				let sugeSet =  {
								"suggessionsDesc": "Tap below to explore", 
								"suggessions": messageService.getSuggesionMessage(allMessageSugestions.suggessions)
							   };
				sharedService.playIncomingMsgAudio();
				messageService.updateMessage({
					message:  sugeSet,
					messageType: 'suggestionMessage',
					userType: 'suggestion',
					timeStamp: sharedService.getDatetime(),
					error: false
				});

				/*
				messageService.updateMessage({
					message: data.message[3],
					messageType: 'suggestionMessage',
					userType: 'suggestion',
					timeStamp: sharedService.getDatetime(),
					error: false
				});

				messageService.updateMessage({
					message: data.message[4],
					messageType: 'suggestionMessage',
					userType: 'suggestion',
					timeStamp: sharedService.getDatetime(),
					error: false
				});
*/
			}, 1 * 2000);
		});

		// ===================listen socket connection for connect_failed===================
		this.socket.on('connect_failed', () => {
			messageService.updateMessage({
				message: 'Oops. A thing or two went wrong on our end and we couldn’t complete your request. Please try again.',
				messageType: 'message',
				userType: 'bot',
				timeStamp: sharedService.getDatetime(),
				error: false
			});
			console.log('6.1: listening connect_failed');
		});
		// this.msgService.updateMessage(null);

		// ===================listen socket connection for Error===================
		this.socket.on('Error', () => {
			messageService.updateMessage({
				message: 'Oops. A thing or two went wrong on our end and we couldn’t complete your request. Please try again.',
				messageType: 'message',
				userType: 'bot',
				timeStamp: sharedService.getDatetime(),
				error: false
			});
			console.log('6.1: listening Error');
		});

		// ===================listen socket connection for new message===================
		this.socket.on('new message', function(data) {
			console.log('Data : \n ' + JSON.stringify(data));
			sessionStorage.setItem('sessionID', data.sessionID);
			this.showLoad = false;

			
			sessionStorage.setItem('intentFulfilled',data.intentFulfilled);		

			// let intentFulfilled = true;

			let allMessageSugestions:any = messageService.getAllSuggestionMsgs();	
			let messageFromServer: string = data.message; 

			let multiMessagesReceived = messageFromServer.split('\\r');
			let multiMessages= [];

			let delaySecond = 0;
			
			multiMessagesReceived.forEach((msgItem, itemIndex) => {
				if(msgItem.trim()===''){
					return;
				}

				multiMessages.push(msgItem);
			});

			multiMessages.forEach((msgItem: string, itemIndex) => {

				delaySecond = delaySecond + 1500;
				let botProgress = '';


				if (itemIndex !== multiMessages.length - 1) {
					botProgress =  '_BOT_PROGRESS';
				}


				console.log('delaySecond: ' + delaySecond);
				setTimeout(() => {

					// debugger
					// if(msgItem.indexOf('enter zip code')>=0){
					// 	intentFulfilled = false;
					// }
					sharedService.playIncomingMsgAudio();
					sharedService.setLastRecievedMessage(msgItem + botProgress);
					messageService.updateMessage({
						message: msgItem,
						messageType: (data.msgType !== undefined ? data.msgType : 'message'),
						userType: 'bot',
						timeStamp: sharedService.getDatetime(),
						error: false
					});
				}, delaySecond);


				if(itemIndex===multiMessages.length-1){

					
					setTimeout(() => {
						sharedService.setLastRecievedMessage(msgItem+botProgress);
						
					
						let sugeSet =  {
							"suggessionsDesc": "Tap below to explore", 
							"suggessions": messageService.getSuggesionMessage(allMessageSugestions.suggessions)
						   };

						   
						   if(data.intentFulfilled===true ){

								sharedService.playIncomingMsgAudio();
								messageService.updateMessage({
									message:  sugeSet,
									messageType: 'suggestionMessage',
									userType: 'suggestion',
									timeStamp: sharedService.getDatetime(),
									error: false
								});
						   }
					}, delaySecond + 4000);
					
				}

			});

			console.log('7: listening new message from watson : \n ' + data.message);
			// console.log('6: Welcome message type  '+ (data.msgType != undefined?data.msgType:"message"));

		});

		this.socket.on('disableChat', function(data) {
			this.showLoad = false;
			console.log('7: listening new message disableChat(socket.on) ' + data.message);
			// sharedService.setModalParams(data.message, true);
			messageService.updateMessage({
				message: data.message,
				messageType: 'errorMsg',
				userType: '',
				timeStamp: sharedService.getDatetime(),
				error: false,
				userName: ''
			});
			sharedService.setDisableMessage(data.message);
		});

		this.socket.on('disconnect', (data) => {
			console.log('****************socket disconnect recieved**************');
			this.isDisconnected = true;
			this.disconnectStartTime = new Date();
			console.log('Disconnected at :' + this.disconnectStartTime);
			this.csrWaitingTimer = setTimeout(() => {
				console.log('Network Disconnectivity exceeded idle timeout of SRM Hence Chat ended by timeout');
				messageService.updateMessage({
					message: 'We had a problem with your chat\'s connection..',
					messageType: 'errorMsg',
					userType: '',
					timeStamp: sharedService.getDatetime(),
					error: false,
					userName: ''
				});
				sharedService.setDisableMessage('Connection Issue');
				this.destroy();
			}, 50 * 1000);
			sharedService.setModalParams('ConnectionLost', false);
		});
	}

	public sendMessage(message, flag) {
		const outgoingMsg = {msg: message, sessionID: sessionStorage.getItem('sessionID'),
		intentFulfilled: sessionStorage.getItem('intentFulfilled')};
		this.socket.emit('send message', outgoingMsg, flag);

	}



	openModal(message: string) {
		alert(message);
	}



	// -------------------------
	private loadConfig() {
		// console.log('environment'+env);
		let orchDomain = window['orchUrl'];
		console.log('orchDomain in UI ', orchDomain);
		if (orchDomain === '' || orchDomain === '<%=orchUrl%>') {
			orchDomain = 'http://localhost:3000';
		}
		console.log('orchDomain' + orchDomain);
		this.tokenURL = orchDomain + '/tokencreation';
		this.socketUrl = orchDomain; // 'http://localhost:3000';


	}

	// ==============================Get JWT Token================================
	initSocketConnection(attributes: IAppProperties) {
		console.log('2: Invoked getJwtToken ' + attributes);
		// const win_id = this.getWindowID();
		// const browser_type = this.getbrowserType();

		const jwtTokenParameters = {
			win_id: this.getWindowID(),
			browser_type: this.getbrowserType(),
		};
		const data = jwtTokenParameters;
		const config = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json'
			})
		};

		console.log('3: caht bot here');
		this.connectToSocket(attributes, this.msgService, this.sharedService);
	}

	public preChatFailure(preChatResponse: IAuthResponse, messageService: MessageService, sharedService: SharedService) {
		// sharedService.setModalParams(preChatResponse.preChatFailureMessage, true);

		messageService.updateMessage({
			message: preChatResponse.preChatFailureMessage,
			messageType: 'errorMsg',
			userType: '',
			timeStamp: sharedService.getDatetime(),
			error: false,
			userName: ''
		});
		sharedService.setDisableMessage(preChatResponse.preChatFailureMessage);
	}

	public connectionFailure(messageService: MessageService, sharedService: SharedService) {
		messageService.updateMessage({
			message: 'We had a problem with your chat\'s connection.',
			messageType: 'errorMsg',
			userType: '',
			timeStamp: sharedService.getDatetime(),
			error: false,
			userName: ''
		});
		this.sharedService.setDisableMessage('Connection Issue');
	}

	public ffqChatbotConnectionFailure(messageService: MessageService, sharedService: SharedService) {
		messageService.updateMessage({
			message: 'Oops. A thing or two went wrong on our end and we couldn’t complete your request. Please try again.',
			messageType: 'orchErrorMsg',
			userType: '',
			timeStamp: '',
			error: true,
			userName: ''
		});
		this.sharedService.setConnectionFailure(true);
	}


	getbrowserType() {
		const ua = navigator.userAgent;
		let tem;
		let M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
		if (/trident/i.test(M[1])) {
			tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
			return 'IE ' + (tem[1] || '');
		}
		if (M[1] === 'Chrome') {
			tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
			if (tem != null) { return tem.slice(1).join(' ').replace('OPR', 'Opera'); }
		}
		M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
		// tslint:disable-next-line: no-conditional-assignment
		if ((tem = ua.match(/version\/(\d+)/i)) != null) { M.splice(1, 1, tem[1]); }
		return M.join(' ');
	}

	// ===================================Get WindowID ================================
	guid() {

		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
				.toString(16)
				.substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	}

	getWindowID() {
		const windowID = this.guid();
		return windowID;
	}

	getDatetime() {
		const date = new Date();
		let hours: any = date.getHours();
		let minutes: any = date.getMinutes();
		const ampm = hours >= 12 ? 'PM' : 'AM';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? '0' + minutes : minutes;
		hours = hours < 10 ? '0' + hours : hours;
		const strTime = hours + ':' + minutes + '' + ampm;
		return strTime;
	}

	/**
	 * [destroy description]
	 * @return [type] [description]
	 */
	destroy() {
		if (this.socket) {
			this.socket.removeAllListeners();
			this.socket.disconnect();
			this.socket = undefined;
		}
	}
}
