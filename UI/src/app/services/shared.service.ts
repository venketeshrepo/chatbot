import { Subject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { IMessageDetails } from '../model/message-details';


@Injectable({
		providedIn: 'root'
})

export class SharedService {

		private conatinerHeight = new Subject<any>();
		private modalParams = new Subject<any>();
		private isChatEnded = new Subject<any>();
		private sendMessage = new Subject<any>();
		private disableMessage = new Subject<any>();
		private connectionFailure = new Subject<any>();
		private csrTransferStatus = new Subject<any>();
		private agentTypingStatus = new Subject<any>();
		private lastRecievedMessage = new Subject<any>();

		constructor() {
		}


		/**
			* [setagentTypingStatus description]
			* @param [type] status [description]
			*/
		setAgentTypingStatus(status) {
			this.agentTypingStatus.next(status);
		}
		/**
			* [getagentTypingStatus description]
			* @return [type] [description]
 		*/
		getAgentTypingStatus() {
 			return this.agentTypingStatus.asObservable();
		}

		/**
			* [setCsrTransferStatus description]
			* @param [type] status [description]
			*/
		setCsrTransferStatus(status) {
			this.csrTransferStatus.next(status);
		}
		/**
			* [getCsrTransferStatus description]
			* @return [type] [description]
 		*/
		getCsrTransferStatus() {
 			return this.csrTransferStatus.asObservable();
		}

		setModalParams(message, flag) {

			this.modalParams.next({message, flag});
		}

		getModalParams(): Observable<any> {
			return this.modalParams.asObservable();
		}

		openModal(message) {
			alert(message);
			// modalService.open(id);
		}


		getContainerHeight(): Observable<any> {
				return this.conatinerHeight.asObservable();
		}

		setContainerHeight(height: any) {
				this.conatinerHeight.next(height);
		}

		getSendMessage(): Observable<any> {
			console.log('inside getSendMessage');
			return this.sendMessage.asObservable();
		}
		setSendMessage(msg: any) {
			console.log('inside setSendMessage');
			this.sendMessage.next(msg);
		}

		setDisableMessage(msg: any) {
			console.log('inside setReceiveMessage');
			this.disableMessage.next(msg);
		}

		getDisableMessage(): Observable<any> {
			console.log('inside getReceiveMessage');
			return this.disableMessage.asObservable();
		}

		setConnectionFailure(msg: any) {
			this.connectionFailure.next(msg);
		}

		getConnectionFailure(): Observable<any> {
			return this.connectionFailure.asObservable();
		}

		setLastRecievedMessage(msg: any) {
			this.lastRecievedMessage.next(msg);
		}

		getLastRecievedMessage(): Observable<any> {
			return this.lastRecievedMessage.asObservable();
		}


		/**
		 * [setIsChatEnded description]
		 * @param [type] endchatStatus [description]
		 */
		setIsChatEnded(endchatStatus) {
			this.isChatEnded.next(endchatStatus);
		}
		/**
		 * [getIsChatEnded description]
		 * @param [type] endchatStatus [description]
		 */
		getIsChatEnded() {
			return this.isChatEnded.asObservable();
		}

		getDatetime() {
				const date = new Date();
				let hours: any = date.getHours();
				let minutes: any = date.getMinutes();
				const ampm = hours >= 12 ? 'PM' : 'AM';
				hours = hours % 12;
				hours = hours ? hours : 12; // the hour '0' should be '12'
				minutes = minutes < 10 ? '0' + minutes : minutes;
				hours = hours < 10 ? '0' + hours : hours;
				const strTime = hours + ':' + minutes + '' + ampm;
				return strTime;
		}

		playIncomingMsgAudio(){
			let audio = new Audio();
			audio.src = "/assets/audio/receive.mp3";
			audio.load();
			audio.play();
		}


		playSendMsgAudio(){
			let audio = new Audio();
			audio.src = "/assets/audio/send.mp3";
			audio.load();
			audio.play();
		}			
}
