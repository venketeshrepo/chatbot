import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

import { AngularMaterialModule } from './shared/modules/angular-material/angular-material.module';
import { FormsModule } from '@angular/forms';
import { reducer } from '../store/app.reducer';
import { StoreModule, Store } from '@ngrx/store';

describe('AppComponent', () => {

// let myMockWindow: Window = <any>{'appName':'chatbot' };
	// let myMockWindow= [{'appName':'chatbot'}];
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule,
				AngularMaterialModule, FormsModule
				, StoreModule.forRoot({ appData: reducer })
			],
			declarations: [
				AppComponent
			],
			providers: []
		}).compileComponents();
	}));

	// fit('should create the app', () => {
	// 	const fixture = TestBed.createComponent(AppComponent);
	// 	const app = fixture.debugElement.componentInstance;

	// 	window.appName = 'livechat';

	// 	expect(app).toBeTruthy();
	// });

	it('should create the app', () => {
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;

		expect(app).toBeTruthy();
	});

	it(`should have as title 'ffq-chat-ui'`, () => {
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app.title).toEqual('ffq-chat-ui');
	});

	it('should render title in a h1 tag', () => {
		const fixture = TestBed.createComponent(AppComponent);
		fixture.detectChanges();
		const compiled = fixture.debugElement.nativeElement;
		expect(compiled.querySelector('h1').textContent).toContain('Welcome to ffq-chat-ui!');
	});
});
