import 'hammerjs';
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
  // To disable all the Console.logs in the Production Mode. ie ng build is with --prod
  if (window) {
    // tslint:disable-next-line:only-arrow-functions
    window.console.log = function() { };
  }
}

platformBrowserDynamic().bootstrapModule(AppModule)
	.catch(err => console.error(err));
