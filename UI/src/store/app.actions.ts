import { Action } from '@ngrx/store';

export enum AppActionTypes {
	SendMessage = '[Chat-App] Send Message',
	RecieveMessage = '[Chat-App] Receive Message',
	SetChatContext = '[Chat-App] Set Chat Context'
}

export class SendMessage implements Action {
	readonly type = AppActionTypes.SendMessage;
	constructor(public payload: any) {}
}
export class RecieveMessage implements Action {
	readonly type = AppActionTypes.RecieveMessage;
	constructor(public payload: any) {}
}
export class SetChatContext implements Action {
	readonly type = AppActionTypes.SetChatContext;
	constructor(public payload: any) {}
}
export type AppActions =
	| SendMessage
	| RecieveMessage
	| SetChatContext;
