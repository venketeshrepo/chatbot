import { AppActionTypes, AppActions } from './app.actions';
import { AppContext, AppData } from '../app/model/app.context';
import { Action } from '@ngrx/store';

export const initialState: AppData = {
		context: {
			chatBotContext: null,
			liveChatContext: null,
			appName: ''
		},
		messages: []
	};


export function reducer(
	state: AppData = initialState,
	action: any): AppData {
	switch (action) {
		case AppActionTypes.SendMessage:
			return { ...state, messages: action.payload };
			case AppActionTypes.RecieveMessage:
			return { ...state, messages: action.payload };
			case AppActionTypes.SetChatContext:
			return { ...state, context: action.payload };
		default:
			return state;
	}
}
