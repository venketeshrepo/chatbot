

/* Selectors */
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppData } from '../app/model/app.context';

const getAppStateSelector = createFeatureSelector<AppData>(
	'appData'
);

export const getMessages = createSelector(
	getAppStateSelector,
	state => state.messages
);

export const getContext = createSelector(
	getAppStateSelector,
	state => state.context
);
