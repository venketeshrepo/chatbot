var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
var port = process.env.PORT || 8080;
app.listen(port, () => { logger.info("server started on port: ", port) });
var handlerMapper = require('./core/handlerMapper.js');
var log4js = require('log4js');
var logger = log4js.getLogger();
logger.level = process.env.logLevel || 'debug';
global.logger = logger;

app.post('/handle', (req, res) => {
    logger.info("<------Incoming request----->");
    handlerMapper.execute(req, res);
    // logger.info("Response : --------->", JSON.stringify(res));
});

