const GLOBAL_SUMMARY_URL = 'https://api.covid19api.com/summary';
var request = require('../core/ExternalApiRequester.js');

module.exports = {
    "canHandle" : function(req) {
      return req.body.Intent == 'Covid_Case_Count';
    },
    "handle" : function(req, res) {
       request.get(GLOBAL_SUMMARY_URL)
              .then((data)=> {
                res.send({"params": req.body, intentFulfilled:true, "text": generateMessage(data)});
              })
              .catch((error) => {
                res.send({"params": req.body, intentFulfilled:true, "text": null});
              });
    }
}

function generateMessage(data) {
  return message = "Today " + formatNumber(data.Global.NewConfirmed) +" have been infected & total cases to " + formatNumber(data.Global.TotalConfirmed) + "\\rGood news is "+ formatNumber(data.Global.TotalRecovered) + " people have recovered as of today";
}

function formatNumber(number) {
  return number.toLocaleString();
}