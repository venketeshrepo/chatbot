const ZIP_TO_STATE = 'http://api.zippopotam.us/us/';
var request = require('../core/ExternalApiRequester.js');
const STATE_ENDPOINT = 'https://covidtracking.com/api/states?state=<STATENAME>';

module.exports = {
    "canHandle": function (req) {
        return req.body.Intent == 'Covid_united_states_count';
    },
    "handle": function (req, res) {
        console.log('Covid_united_states_count HANDLE FUNCTION');
        console.log('Request ->', JSON.stringify(req.body));
        let zip_url = ZIP_TO_STATE + JSON.parse(req.body.slots).zip_code;
        request.get(zip_url)
            .then((data) => {
                const stateDetails = data.places[0];
                let stateAbbrevation = stateDetails["state abbreviation"];
                let stateURL = STATE_ENDPOINT.replace('<STATENAME>', stateAbbrevation);
                return request.get(stateURL)
                    .then((data) => {
                        // console.log(JSON.stringify(data));
                        // console.log('FINAL RESPONSE -> ',JSON.stringify({ "params": req.body, intentFulfilled: true, "text": generateMessage(data) }));
                        res.send({ "params": req.body, intentFulfilled: true, "text": generateMessage(data) });
                    })

            })
            .catch((error) => {
                console.log(error);
                res.send({ "params": req.body, intentFulfilled: true, "text": "sorry couldn't able to find the details now. Please try again later" });
            });
    }
}

function generateMessage(data) {
    // console.log(JSON.stringify(data));
    let response = "Yeah!!!! We have got the results for the requested state\\r";
    // console.log(response);
    response = response + "As of today, your state " + data.state + " have " + formatNumber(data.positive) + " people infected with Cov19.\\r";
    // console.log(response);
    if (formatNumber(data.death)) {
        response = response + " Deaths have been accounted to " + formatNumber(data.death) + " as of today.\\r";
    }
    // console.log(response);
    if (data.recovered) {
        response = response + "Good new is " + formatNumber(data.recovered) + " have been recoved from novel corona virus.";
    }
    // console.log(response);
    return response;
}


function formatNumber(number) {
    return number.toLocaleString();
}