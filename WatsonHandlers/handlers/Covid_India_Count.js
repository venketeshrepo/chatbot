const ZIP_TO_STATE = 'https://api.postalpincode.in/pincode/';
var request = require('../core/ExternalApiRequester.js');
const STATE_ENDPOINT = 'https://api.covid19india.org/data.json';

module.exports = {
    "canHandle": function (req) {
        return req.body.Intent == 'Covid_India_Count';
    },
    "handle": function (req, res) {
        console.log('Covid_india HANDLE FUNCTION');
        console.log('Request ->', JSON.stringify(req.body));
        let zip_url = ZIP_TO_STATE + JSON.parse(req.body.slots).zip_code;
        let stateData={};
        request.get(zip_url)
            .then((data) => {
                const stateName = data[0].PostOffice[0].State;
                return request.get(STATE_ENDPOINT)
                    .then((data) => {
                        const stateWiseDetails=data.statewise;
                        for(let st of stateWiseDetails) {
                            if(st.state==stateName){
                             stateData =st;  
                             break;   
                            }
                        }

                        res.send({ "params": req.body, intentFulfilled: true, "text": generateMessage(stateData) });
                    })

            })
            .catch((error) => {
                console.log(error);
                res.send({ "params": req.body, intentFulfilled: true, "text": "sorry couldn't able to find the details now. Please try again later" });
            });
    }
}

function generateMessage(data) {
    // console.log(JSON.stringify(data));
    let response = "Yeah!!!! We have got the results for the requested state\\r";
    // console.log(response);
    response = response + "As of today, your state " + data.state + " have " + formatNumber(data.confirmed) + " people infected with Cov19.\\r";
    // console.log(response);
    if (formatNumber(data.deaths)) {
        response = response + " Deaths have been accounted to " + formatNumber(data.deaths) + " as of today.\\r";
    }
    // console.log(response);
    if (data.recovered) {
        response = response + "Good news is " + formatNumber(data.recovered) + " have been recoved from novel corona virus.";
    }
    // console.log(response);
    return response;
}


function formatNumber(number) {
    return number.toLocaleString();
}