const ZIP_TO_STATE = 'http://api.zippopotam.us/us/';
const TEST_LOCATION =  'https://covid-19-testing.github.io/locations/<STATENAME>/complete.json';
var request = require('../core/ExternalApiRequester.js');

module.exports = {
    "canHandle" : function(req) {
      return req.body.Intent == 'Covid_Testing_Locations';
    },
    "handle" : function(req, res) {
        let location_code = JSON.parse(req.body.slots).location_code;
        let zip_url = ZIP_TO_STATE + location_code;
       request.get(zip_url)
              .then((data)=> {
                let stateName = data.places[0].state.toLowerCase().replace(/\s/g, "-");
                let loc_url = TEST_LOCATION.replace('<STATENAME>', stateName);
                return request.get(loc_url)
                               .then((data) => {
                                res.send({"params": req.body, intentFulfilled:true, "text" : generateMessage(data, stateName)});
                               })
              })
              .catch((error) => {
                res.send({"params": req.body, intentFulfilled:true, "text" : "sorry couldn't able to find nearby clinic for your location"});
              });
    }
}

function generateMessage(data, stateName) {
    let response = "Here are the available locations for the "+stateName+"\\r";
     for(let x in data) {
        response = response + "Name : "+ data[x].name+"\\n";
        response = response + "Address : \\n";
        response = response + data[x].physical_address[0].address_1+" , ";
        response = response + data[x].physical_address[0].city+ "\\n";
        response = response + "Zip Code : " + data[x].physical_address[0].postal_code + "\\n";
        response = response + "Phone :"+ data[x].phones[0].number+"\\n";
        // response = response + data[x].phones[0].number+"\\n";
        if(x == 1) {
          break;
        }
        else {
          response = response + "\\r";
        }
     }
    return response;
}