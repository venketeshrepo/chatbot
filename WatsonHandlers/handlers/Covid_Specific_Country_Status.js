var request = require('../core/ExternalApiRequester.js');
const countryWiseCovidSummaryEndpoint = 'https://api.covid19api.com/summary';

module.exports = {
    "canHandle": function (req) {
        return req.body.Intent == 'Covid_specific_country_status';
    },
    "handle": function (req, res) {
        request.get(countryWiseCovidSummaryEndpoint)
            .then((data) => {
                // console.log(data);
                const countryNameRequested = JSON.parse(req.body.slots).country;
                const countriesArray = data.Countries;
                let responseFound = false;
                for (var i = 0; i < countriesArray.length; i++) {
                    const countryNamefromApi = countriesArray[i].Country;
                    if ((countryNamefromApi.toLowerCase().replace(/\s/g, '') === countryNameRequested.toLowerCase().replace(/\s/g, '')) || 
                    countryNameRequested.toLowerCase().replace(/\s/g, '') === countriesArray[i].Slug.toLowerCase().replace(/\s/g, '') ||
                    countryNameRequested.toLowerCase().replace(/\s/g, '') === countriesArray[i].CountryCode.toLowerCase().replace(/\s/g, '')) {
                        console.log('Returning status of the country ----->', countryNamefromApi);
                        responseFound = true;
                        res.send({ "params": req.body, intentFulfilled: true, "text": generateMessage(countriesArray[i]) });
                    }
                }
                if(!responseFound) {
                    res.send({ "params": req.body, intentFulfilled: true, 
                    "text": "Sorry Couldn't find the details of covid status for the requested country "+ countryNameRequested});
                }
            })
    }
}

function generateMessage(data) {
    console.log('Data fetched from response ----->', JSON.stringify(data));
    let response = "Yeah!!!! We have got the results for the requested country\\r";
    response = response + "As of today, requested country " + data.Country + " have " + formatNumber(data.TotalConfirmed) + " total confirmed covid19 cases.\\r"
    if (data.TotalDeaths) {
        response = response + formatNumber(data.TotalDeaths) + " is the total number of deaths reported.\\r"
    }
    if (data.TotalRecovered) {
        response = response + formatNumber(data.TotalRecovered) + " is the total recovered from the novel corona virus.\\r"
    }
    return response;
} 


function formatNumber(number) {
    return number.toLocaleString();
}