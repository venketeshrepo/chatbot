var request = require('../core/ExternalApiRequester.js');
var endPoint = 'https://tools.cdc.gov/api/v2/resources/media/132608.rss';
var parseString = require('xml2js').parseString;
// fs = require('fs');

module.exports = {
    "canHandle": function (req) {
        return req.body.Intent == 'CDC_Mail_Room_News';
    },
    "handle": function (req, res) {
        request.get(endPoint)
            .then((xmldata) => {
                // console.log(xmldata);
                parseString(xmldata, function (error, result) {
                    if (error && error !== undefined) {
                        res.send({ "params": req.body, intentFulfilled: true, "text": "There is no latest news published by CDC. Please try latest. Thanks." });
                    } else {
                        const newsObject = result.rss.channel[0];
                        var response = generateResponse(newsObject);
                        res.send({ "params": req.body, intentFulfilled: true, "text": response });
                    }

                });
            })
    }
}
function generateResponse(newsObject) {
    let response = "We bring you latest news from CDC related to COVID-19\\r";
    const itemArray = newsObject.item;
    for (var i = 0; i < itemArray.length; i++) {
        const categoryArray = itemArray[i].category;
        for (var j = 0; j < categoryArray.length; j++) {
            if (categoryArray[j] === 'COVID-19') {
                // console.log(itemArray[i].description);
                response = response + itemArray[i].title[0] + "\\n";
                response = response + itemArray[i].description[0] + "\\n";
                response = response + itemArray[i].link[0] + "\\n"
                response = response + "Published Date: " + itemArray[i].pubDate[0] + "\\r"
            }

        }
        if (i == 2) {
            break;
        }
    }
    return response;
}