const reponse_msg =  "STAY home \\n KEEP a safe distance \\nWASH hands often \\nCOVER your cough";
module.exports = {
    "canHandle" : function(req) {
       return req.body.Intent == 'Covid_Protecting_Against_Infection';
    },
    "handle" : function(req, res) {
        res.send({"params": req.body, intentFulfilled:true, "text" : reponse_msg});
    }
}
