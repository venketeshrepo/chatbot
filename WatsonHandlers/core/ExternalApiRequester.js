var request = require('request-promise');

module.exports = {
    "get" : function(url) {
        logger.info("URL - get request:-", url);
        let options = {
            uri:url,
            json:true
        }
        return request(options);
    },
    "post" : function(url, body) {
        logger.info("URL - post request:-", url);
        logger.debug("Post request body:-", JSON.stringify(body));
        let options = {
            uri:url,
            body: body,
            method: "POST",
            json:true
        }
        return request(options);
    }
}