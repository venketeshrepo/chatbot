var Covid_Case_Count = require('../handlers/Covid_Case_Count.js');
var Covid_Protecting_Against_Infection = require('../handlers/Covid_Protecting_Against_Infection.js');
var Covid_Testing_Locations = require('../handlers/Covid_Testing_Locations.js');
var Covid_United_States_Count = require('../handlers/Covid_United_States_Count.js');
var Covid_Specific_Country_Status = require('../handlers/Covid_Specific_Country_Status.js');
var CDC_News_room = require('../handlers/CDC_News_room.js');
var Covid_India_Count = require('../handlers/Covid_India_Count.js');
var errorHandler = require('../handlers/errorHandler.js');
const handlers = [Covid_Case_Count,
                  Covid_Protecting_Against_Infection,
                  Covid_Testing_Locations,
                  Covid_United_States_Count,
                  Covid_Specific_Country_Status,
                  CDC_News_room,
				  Covid_India_Count,
                  errorHandler];

module.exports = {
    "execute" : function (req, res) {
        for(let handler of handlers) {
            if(handler.canHandle(req)) {
                handler.handle(req, res);
                break;
            }
        }
    }
}