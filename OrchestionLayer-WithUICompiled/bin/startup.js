const settings    	= require('../project_settings');
const currentEnv 	= settings.ENVIRONMENT;
const app 			= require('../app');
const serverPort 	= settings.SERVER.PORT;
const currentPort 	= process.env.PORT || serverPort;

console.log('PORT', process.env.PORT);
console.log('serverPort', serverPort);
console.log('currentPort', currentPort);

if (currentPort) {
	app.set('port', currentPort);

	//app.listen(app.get('port'), function() {
	app.listen(currentPort, function() {
		console.log('DLC listening to the port : x');
	});
} else {
	console.log('DLC : the port is not properly configured: x');
}	