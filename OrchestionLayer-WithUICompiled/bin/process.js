const pm2         = require('pm2');
const settings    = require('../project_settings');
const currentEnv  = settings.ENVIRONMENT;
const envName     = 'Chatbot-' + settings.ENVIRONMENT;
const logPath     = (settings.LOGGER.LOGGER_PATH) ? settings.LOGGER.LOGGER_PATH : settings.PROJECT_DIR;

pm2.connect(function() {
    pm2.start({
        'script' : 'bin/startup.js',
        'node_args' : ['--expose-gc'],
        'name' : envName,
        'exec_mode' : 'cluster',
        'instances' : 0,
        'max_memory_restart' : '1G',
        'log_date_format' : 'YYYY-MM-DDTHH:mm:ss.SSS Z',
        'error_file' :  logPath + currentEnv +'_error.log',
        'out_file' : logPath + currentEnv +'.log',
        'merge_logs' : true,
        'pmx' : false
    }, function(err, apps) {
        console.log('=============================err', err);
        console.log('=============================apps', apps);
        pm2.disconnect();
    });
});
