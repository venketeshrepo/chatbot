(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = " <router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/dialog/dialog.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/dialog/dialog.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@dialog] *ngIf=\"visible\" class=\"dialog\">\r\n\t<ng-content></ng-content>\r\n\t<!--<button *ngIf=\"closable\" (click)=\"close()\" aria-label=\"Close\" class=\"dialog__close-btn\">X</button>-->\r\n</div>\r\n<div *ngIf=\"visible\" class=\"overlay\" (click)=\"close()\"></div>"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var routes = [
    { path: 'chatbot', loadChildren: function () { return __webpack_require__.e(/*! import() | ffq-chatbot-module */ "ffq-chatbot-module").then(__webpack_require__.bind(null, /*! ./ffq-chatbot.module */ "./src/app/ffq-chatbot.module.ts")).then(function (m) { return m.FfqChatbotModule; }); } },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _store_app_selectors__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../store/app.selectors */ "./src/store/app.selectors.ts");
/* harmony import */ var _store_app_actions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../store/app.actions */ "./src/store/app.actions.ts");






var AppComponent = /** @class */ (function () {
    function AppComponent(activatedRoute, router, store) {
        var _this = this;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.store = store;
        this.title = 'ffq-chat-ui';
        this.hasRouted = false;
        this.quoteNumber = 'quote_number';
        this.testUrl = 'testUrl';
        this.appName = 'appName';
        this.testEmail = 'test_email';
        this.agentName = 'agent_name';
        this.name = 'name';
        this.lobsQuoted = 'lobs_quoted';
        this.zipCode = 'zip_code';
        this.envUrl = 'environment_url';
        this.residenceType = 'residence_type';
        this.buyOptions = 'buyOptions';
        this.selectedCrossSellLob = 'selectedCrossSellLob';
        this.isLiveChat = 'isLiveChat';
        this.firstName = 'first_name';
        this.lastName = 'last_name';
        this.street = 'street';
        this.apartment = 'apartment';
        this.city = 'city';
        this.state = 'state';
        this.gender = 'gender';
        this.pageName = 'page_name';
        this.context$ = this.store.select(_store_app_selectors__WEBPACK_IMPORTED_MODULE_4__["getContext"]);
        console.log('testUrl' + window[this.testUrl]);
        this.context$.subscribe(function (ctx) { _this.context = ctx; });
        // const subscription = this.activatedRoute.queryParamMap.subscribe(queryParams => {
        if ((!this.hasRouted) && (window[this.appName])) {
            console.log('1 Inside AppComponent : ' + window[this.appName]);
            // this.contextService.setAppName(queryParams.get('appName'));
            // tslint:disable-next-line: no-string-literal
            this.context.appName = window[this.appName];
            // if (this.context.appName === 'chatbot'  || this.context.appName === 'refreshchatbot') {
            // 	console.log('about to updateChatbotContext');
            // 	if (this.context.appName === 'chatbot') {
            // 	this.context.chatBotContext = this.updateChatbotContext();
            // 	this.store.dispatch(new SetChatContext(this.context));
            // 	sessionStorage.setItem('context', JSON.stringify(this.context));
            // } else {
            // 	console.log('setting store with context from session storage chatbot');
            // 	this.store.dispatch(new SetChatContext(JSON.parse(sessionStorage.getItem('context'))));
            // }
            // 	// subscription.unsubscribe();
            // 	this.router.navigate(['/chatbot']);
            // } else if (this.context.appName === 'livechat' || this.context.appName === 'refreshlivechat') {
            // 	console.log('about to updateLivechatContext');
            // 	if (this.context.appName === 'livechat') {
            // 	this.context.liveChatContext =  this.updateLivechatContext();
            // 	this.store.dispatch(new SetChatContext(this.context));
            // 	sessionStorage.setItem('context', JSON.stringify(this.context));
            // } else {
            // 	console.log('setting store with context from session storage live chat');
            // 	this.store.dispatch(new SetChatContext(JSON.parse(sessionStorage.getItem('context'))));
            // 	console.log('context from session storage' + JSON.stringify(JSON.parse(sessionStorage.getItem('context'))));
            // }
            // 	// subscription.unsubscribe();
            // 	this.router.navigate(['/livechat']);
            // } else {
            console.log('Info Mocked############');
            this.context.chatBotContext = this.updateChatbotContext();
            this.store.dispatch(new _store_app_actions__WEBPACK_IMPORTED_MODULE_5__["SetChatContext"](this.context));
            sessionStorage.setItem('context', JSON.stringify(this.context));
            this.router.navigate(['/chatbot']);
            // }
            this.hasRouted = true;
        }
        // });
    }
    AppComponent.prototype.updateChatbotContext = function () {
        console.log('updateChatbotContext()');
        var appProperties = {
            applicationCode: 'FFQUI',
            quote_number: window[this.quoteNumber],
            testUser: (window[this.testEmail] === 'true'),
            token: '',
            agent_name: window[this.agentName] || '',
            name: window[this.name] || 'User',
            application_code: 'FFQUI',
            agent_phone: '',
            user_type: '',
            lobs_quoted: window[this.lobsQuoted],
            zip_code: window[this.zipCode],
            environment_url: window[this.envUrl],
            residence_type: window[this.residenceType] || '',
            buyOptions: window[this.buyOptions],
            selectedCrossSellLobs: window[this.selectedCrossSellLob],
            isLiveChat: (window[this.isLiveChat] === 'Y'),
            userName: window[this.name] || '',
            userFName: '',
            userLName: '',
            street: '',
            apartment: '',
            city: '',
            state: '',
            gender: '',
            page_name: '',
            appName: ''
        };
        // this.contextService.setChatBotContext(appProperties);
        return appProperties;
    };
    AppComponent.prototype.updateLivechatContextMockLocal = function () {
        var appProperties = {
            applicationCode: 'FFQUI',
            quote_number: '1529081971',
            testUser: false,
            token: '',
            agent_name: '',
            name: '',
            application_code: 'FFQUI',
            agent_phone: '',
            user_type: '',
            environment_url: '',
            residence_type: '',
            buyOptions: '',
            selectedCrossSellLobs: '',
            userName: '',
            isLiveChat: true,
            userFName: 'FirstNameMock',
            userLName: 'LastNameMock',
            street: 'StreetMock',
            apartment: 'ApartmentMock',
            city: 'CityMock',
            state: 'StateMock',
            gender: 'MFMOck',
            zip_code: 'zipMock',
            lobs_quoted: 'autoMocked',
            page_name: 'DRIVER',
            appName: 'livechatmock'
        };
        console.log('Customer First Name: ' + appProperties.userFName);
        console.log('Customer Last Name: ' + appProperties.userLName);
        console.log('Gender: ' + appProperties.gender);
        console.log('Apartment: ' + appProperties.apartment);
        console.log('Street: ' + appProperties.street);
        console.log('City: ' + appProperties.city);
        console.log('State: ' + appProperties.state);
        console.log('zip_code: ' + appProperties.zip_code);
        console.log('Page Name: ' + appProperties.page_name);
        // this.contextService.setChatBotContext(appProperties);
        return appProperties;
    };
    AppComponent.prototype.updateLivechatContext = function () {
        var appProperties = {
            applicationCode: 'FFQUI',
            quote_number: window[this.quoteNumber],
            testUser: (window[this.testEmail] === 'true'),
            token: '',
            agent_name: '',
            name: '',
            application_code: 'FFQUI',
            agent_phone: '',
            user_type: '',
            environment_url: window[this.envUrl],
            residence_type: '',
            buyOptions: '',
            selectedCrossSellLobs: '',
            userName: '',
            isLiveChat: (window[this.isLiveChat] === 'Y'),
            userFName: window[this.firstName],
            userLName: window[this.lastName],
            street: window[this.street],
            apartment: window[this.apartment],
            city: window[this.city],
            state: window[this.state],
            gender: window[this.gender],
            zip_code: window[this.zipCode],
            lobs_quoted: window[this.lobsQuoted],
            page_name: window[this.pageName],
            appName: window[this.appName]
        };
        console.log('Customer First Name: ' + appProperties.userFName);
        console.log('Customer Last Name: ' + appProperties.userLName);
        console.log('Gender: ' + appProperties.gender);
        console.log('Apartment: ' + appProperties.apartment);
        console.log('Street: ' + appProperties.street);
        console.log('City: ' + appProperties.city);
        console.log('State: ' + appProperties.state);
        console.log('zip_code: ' + appProperties.zip_code);
        console.log('Page Name: ' + appProperties.page_name);
        // this.contextService.setChatBotContext(appProperties);
        return appProperties;
    };
    AppComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"] }
    ]; };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _store_app_reducer__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../store/app.reducer */ "./src/store/app.reducer.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _shared_modules_angular_material_angular_material_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./shared/modules/angular-material/angular-material.module */ "./src/app/shared/modules/angular-material/angular-material.module.ts");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _ngrx_store_devtools__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ngrx/store-devtools */ "./node_modules/@ngrx/store-devtools/fesm5/store-devtools.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _dialog_dialog_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./dialog/dialog.component */ "./src/app/dialog/dialog.component.ts");















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_7__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"],
                _dialog_dialog_component__WEBPACK_IMPORTED_MODULE_14__["DialogComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_8__["AppRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                _shared_modules_angular_material_angular_material_module__WEBPACK_IMPORTED_MODULE_10__["AngularMaterialModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_11__["FlexLayoutModule"],
                _ngrx_store__WEBPACK_IMPORTED_MODULE_5__["StoreModule"].forRoot({ appData: _store_app_reducer__WEBPACK_IMPORTED_MODULE_6__["reducer"] }),
                _ngrx_store_devtools__WEBPACK_IMPORTED_MODULE_12__["StoreDevtoolsModule"].instrument({
                    name: 'Chatbot DevTools',
                    maxAge: 25,
                    logOnly: _environments_environment__WEBPACK_IMPORTED_MODULE_13__["environment"].production
                }),
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/dialog/dialog.component.css":
/*!*********************************************!*\
  !*** ./src/app/dialog/dialog.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".overlay {\r\n  position: fixed;\r\n  top: 0;\r\n  bottom: 0;\r\n  left: 0;\r\n  right: 0;\r\n  background-color: rgba(0, 0, 0, 0.5);\r\n  z-index: 999;\r\n}\r\n\r\n.dialog {\r\n  z-index: 1000;\r\n  position: absolute;\r\n  top: 35%;\r\n  width: 70%;\r\n  padding: 10px;\r\n  background-color: #c1e2f5;\r\n  border-radius: 6px;\r\n  font-family: Arial;\r\n  font-size: 10px;\r\n  margin-left: 15%;\r\n  margin-top: 8%;\r\n  box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);\r\n}\r\n\r\n@media (min-width: 768px) {\r\n  .dialog {\r\n    top: 25%;\r\n  }\r\n}\r\n\r\n.dialog__close-btn {\r\n  border: 0;\r\n  background: none;\r\n  color: #2d2d2d;\r\n  position: absolute;\r\n  top: 8px;\r\n  right: 8px;\r\n  font-size: 1.2em;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGlhbG9nL2RpYWxvZy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBZTtFQUNmLE1BQU07RUFDTixTQUFTO0VBQ1QsT0FBTztFQUNQLFFBQVE7RUFDUixvQ0FBb0M7RUFDcEMsWUFBWTtBQUNkOztBQUVBO0VBQ0UsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsVUFBVTtFQUNWLGFBQWE7RUFDYix5QkFBeUI7RUFDekIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFHZCw0Q0FBNEM7QUFDOUM7O0FBRUE7RUFDRTtJQUNFLFFBQVE7RUFDVjtBQUNGOztBQUVBO0VBQ0UsU0FBUztFQUNULGdCQUFnQjtFQUNoQixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixVQUFVO0VBQ1YsZ0JBQWdCO0FBQ2xCIiwiZmlsZSI6InNyYy9hcHAvZGlhbG9nL2RpYWxvZy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm92ZXJsYXkge1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICB0b3A6IDA7XHJcbiAgYm90dG9tOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgcmlnaHQ6IDA7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjUpO1xyXG4gIHotaW5kZXg6IDk5OTtcclxufVxyXG5cclxuLmRpYWxvZyB7XHJcbiAgei1pbmRleDogMTAwMDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiAzNSU7XHJcbiAgd2lkdGg6IDcwJTtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNjMWUyZjU7XHJcbiAgYm9yZGVyLXJhZGl1czogNnB4O1xyXG4gIGZvbnQtZmFtaWx5OiBBcmlhbDtcclxuICBmb250LXNpemU6IDEwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDE1JTtcclxuICBtYXJnaW4tdG9wOiA4JTtcclxuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggNXB4IDBweCByZ2JhKDAsMCwwLDAuNzUpO1xyXG4gIC1tb3otYm94LXNoYWRvdzogMHB4IDJweCA1cHggMHB4IHJnYmEoMCwwLDAsMC43NSk7XHJcbiAgYm94LXNoYWRvdzogMHB4IDJweCA1cHggMHB4IHJnYmEoMCwwLDAsMC43NSk7XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xyXG4gIC5kaWFsb2cge1xyXG4gICAgdG9wOiAyNSU7XHJcbiAgfVxyXG59XHJcblxyXG4uZGlhbG9nX19jbG9zZS1idG4ge1xyXG4gIGJvcmRlcjogMDtcclxuICBiYWNrZ3JvdW5kOiBub25lO1xyXG4gIGNvbG9yOiAjMmQyZDJkO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDhweDtcclxuICByaWdodDogOHB4O1xyXG4gIGZvbnQtc2l6ZTogMS4yZW07XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/dialog/dialog.component.ts":
/*!********************************************!*\
  !*** ./src/app/dialog/dialog.component.ts ***!
  \********************************************/
/*! exports provided: DialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DialogComponent", function() { return DialogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");



var DialogComponent = /** @class */ (function () {
    function DialogComponent() {
        this.closable = true;
        this.visibleChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    DialogComponent.prototype.ngOnInit = function () { };
    DialogComponent.prototype.close = function () {
        this.visible = false;
        this.visibleChange.emit(this.visible);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DialogComponent.prototype, "closable", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], DialogComponent.prototype, "visible", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], DialogComponent.prototype, "visibleChange", void 0);
    DialogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dialog',
            template: __webpack_require__(/*! raw-loader!./dialog.component.html */ "./node_modules/raw-loader/index.js!./src/app/dialog/dialog.component.html"),
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["trigger"])('dialog', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["transition"])('void => *', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ transform: 'scale3d(.3, .3, .3)' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["animate"])(100)
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["transition"])('* => void', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["animate"])(100, Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ transform: 'scale3d(.0, .0, .0)' }))
                    ])
                ])
            ],
            styles: [__webpack_require__(/*! ./dialog.component.css */ "./src/app/dialog/dialog.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DialogComponent);
    return DialogComponent;
}());



/***/ }),

/***/ "./src/app/shared/modules/angular-material/angular-material.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/shared/modules/angular-material/angular-material.module.ts ***!
  \****************************************************************************/
/*! exports provided: AngularMaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AngularMaterialModule", function() { return AngularMaterialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/esm5/expansion.es5.js");





var AngularMaterialModule = /** @class */ (function () {
    function AngularMaterialModule() {
    }
    AngularMaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDividerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatLineModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"],
                _angular_material_expansion__WEBPACK_IMPORTED_MODULE_4__["MatExpansionModule"]
            ],
            exports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDividerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatLineModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"],
                _angular_material_expansion__WEBPACK_IMPORTED_MODULE_4__["MatExpansionModule"]
            ]
        })
    ], AngularMaterialModule);
    return AngularMaterialModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
    // To disable all the Console.logs in the Production Mode. ie ng build is with --prod
    if (window) {
        // tslint:disable-next-line:only-arrow-functions
        window.console.log = function () { };
    }
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ "./src/store/app.actions.ts":
/*!**********************************!*\
  !*** ./src/store/app.actions.ts ***!
  \**********************************/
/*! exports provided: AppActionTypes, SendMessage, RecieveMessage, SetChatContext */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppActionTypes", function() { return AppActionTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendMessage", function() { return SendMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecieveMessage", function() { return RecieveMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SetChatContext", function() { return SetChatContext; });
var AppActionTypes;
(function (AppActionTypes) {
    AppActionTypes["SendMessage"] = "[Chat-App] Send Message";
    AppActionTypes["RecieveMessage"] = "[Chat-App] Receive Message";
    AppActionTypes["SetChatContext"] = "[Chat-App] Set Chat Context";
})(AppActionTypes || (AppActionTypes = {}));
var SendMessage = /** @class */ (function () {
    function SendMessage(payload) {
        this.payload = payload;
        this.type = AppActionTypes.SendMessage;
    }
    SendMessage.ctorParameters = function () { return [
        { type: undefined }
    ]; };
    return SendMessage;
}());

var RecieveMessage = /** @class */ (function () {
    function RecieveMessage(payload) {
        this.payload = payload;
        this.type = AppActionTypes.RecieveMessage;
    }
    RecieveMessage.ctorParameters = function () { return [
        { type: undefined }
    ]; };
    return RecieveMessage;
}());

var SetChatContext = /** @class */ (function () {
    function SetChatContext(payload) {
        this.payload = payload;
        this.type = AppActionTypes.SetChatContext;
    }
    SetChatContext.ctorParameters = function () { return [
        { type: undefined }
    ]; };
    return SetChatContext;
}());



/***/ }),

/***/ "./src/store/app.reducer.ts":
/*!**********************************!*\
  !*** ./src/store/app.reducer.ts ***!
  \**********************************/
/*! exports provided: initialState, reducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initialState", function() { return initialState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reducer", function() { return reducer; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _app_actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.actions */ "./src/store/app.actions.ts");


var initialState = {
    context: {
        chatBotContext: null,
        liveChatContext: null,
        appName: ''
    },
    messages: []
};
function reducer(state, action) {
    if (state === void 0) { state = initialState; }
    switch (action) {
        case _app_actions__WEBPACK_IMPORTED_MODULE_1__["AppActionTypes"].SendMessage:
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, state, { messages: action.payload });
        case _app_actions__WEBPACK_IMPORTED_MODULE_1__["AppActionTypes"].RecieveMessage:
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, state, { messages: action.payload });
        case _app_actions__WEBPACK_IMPORTED_MODULE_1__["AppActionTypes"].SetChatContext:
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, state, { context: action.payload });
        default:
            return state;
    }
}


/***/ }),

/***/ "./src/store/app.selectors.ts":
/*!************************************!*\
  !*** ./src/store/app.selectors.ts ***!
  \************************************/
/*! exports provided: getMessages, getContext */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMessages", function() { return getMessages; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getContext", function() { return getContext; });
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* Selectors */

var getAppStateSelector = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createFeatureSelector"])('appData');
var getMessages = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getAppStateSelector, function (state) { return state.messages; });
var getContext = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(getAppStateSelector, function (state) { return state.context; });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\Develop\IBM-Hackathon\chatbot\UI\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es5.js.map