(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ffq-livechat-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/dialog/dialog.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/dialog/dialog.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@dialog] *ngIf=\"visible\" class=\"dialog\">\r\n\t<ng-content></ng-content>\r\n\t<!--<button *ngIf=\"closable\" (click)=\"close()\" aria-label=\"Close\" class=\"dialog__close-btn\">X</button>-->\r\n</div>\r\n<div *ngIf=\"visible\" class=\"overlay\" (click)=\"close()\"></div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/livechat-home/chat-header/chat-header.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/livechat-home/chat-header/chat-header.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div #chatHeader>\r\n\t<h6 [ngClass]=\"['pt-2','pb-0','text-left','ml-4','font-weight-bold','mt-1','main-header']\">Covid Chat Bot</h6>\r\n\t<p [ngClass]=\"['mb-2','text-left','ml-4','sub-header']\">We are here to help you..</p>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/livechat-home/chat-messages/chat-messages.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/livechat-home/chat-messages/chat-messages.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<img [hidden]=\"!(messages.length===0)\"  id=\"load-msg\" src=\"../assets/icons/loading_image.gif\" height=\"50\" width=\"50\"/> \r\n<div #chatMessages id=\"scroll\">\r\n\t<mat-list>\r\n\t\t<!-- <mat-list-item class=\"welcomeMsg\">\r\n\t\t\t<div matLine *ngIf=\"welcomeMsg?.message\" class=\"message incomingMessage text-left\">{{welcomeMsg?.message}}</div>\r\n\t\t\t<p matLine *ngIf=\"errorMsgDisplay?.message\" class=\"message incomingMessage text-left\">{{errorMsgDisplay?.message}}</p>\r\n\t\t</mat-list-item> -->\r\n\t\t<mat-list-item *ngFor=\"let data of messages\">\r\n\t\t\t<div matLine [ngClass]=\"data.messageType === 'outgoingMsg' ? ['text-right','outgoingMessage','message-out','contentalign'] : ['text-left','incomingMessage', 'message']\">{{data.message}}\r\n\t\t\t</div>\r\n\t\t\t<div matLine [ngClass]=\"data.messageType === 'outgoingMsg'? ['text-right', 'timestampWrapper'] : 'text-left'\">\r\n\t\t\t\t<span [ngClass]=\"data.messageType === 'outgoingMsg'? ['text-right','userNameAndTimeStamp'] : ['text-left','userNameAndTimeStamp','ml-2']\">{{data.userName}}</span>\r\n\t\t\t\t<span class=\"userNameAndTimeStamp\" [ngClass]=\"data.messageType === 'outgoingMsg' ? ['text-right','mr-2','ml-2'] : ['text-left','ml-2']\">{{data.timeStamp}}</span>\r\n\t\t\t</div>\r\n\t\t</mat-list-item>\r\n\t</mat-list>\r\n<div id=\"status\" class=\"typingStatusDiv\" *ngIf=\"agentTyping;then typing else notTyping\"> </div>\r\n<ng-template #typing><div><p class=\"typingStatus\">{{agentTypingMessage}}</p></div></ng-template>\r\n<ng-template #notTyping> </ng-template>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/livechat-home/chat-sender/chat-sender.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/livechat-home/chat-sender/chat-sender.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [ngClass]=\"disableMessageBox ? ['row chatMessagGreyOut'] : ['row rowWrapper']\">\r\n    <div class=\"col-12 col-sm-12 col-lg-12 col-md-12 col-xl-12 colWrapper\">\r\n        <mat-divider></mat-divider>\r\n        <div [ngClass]=\"disableMessageBox ? ['chatMessageWrapperGrey'] : ['chatMessageWrapper']\">\r\n            <textarea name=\"messageToSend\" id=\"messageToSend\" [disabled]=\"(messages.length===0) || disableMessageBox\" [ngClass]=\"disableMessageBox ? ['txtAreaTypeGrey'] : ['txtAreaType']\" rows=\"3\" placeholder=\"Send us a message ...\" maxlength=\"250\" (keydown)=\"onKeyDown($event)\" [(ngModel)]=\"userMessage\">\r\n            </textarea>            \r\n            <button [disabled]=\"!enableSendButton\" (click)=\"sendMessage()\" [hidden]=\"disableMessageBox\"></button>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/livechat-home/livechat-home.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/livechat-home/livechat-home.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"height: 100%;text-align:center;overflow-y: hidden;\" [ngClass]=\"{'disabled': true}\">\r\n             \r\n          <div #liveChatContainer class=\"container\" fxFlexFill fxLayout=\"column\">\r\n             <!-- chat-header begins -->      \r\n               <div #liveChatHeader class=\"chatHeader\" fxFlex=\"8%\" fxFlex.lg=\"8%\" fxFlex.md=\"8%\" fxFlex.sm=\"8%\" fxFlex.xs=\"15%\">\r\n                 <app-chat-header #chatHeader></app-chat-header>  \r\n               </div>\r\n               <!-- chat-header end -->\r\n\r\n            <!-- chat-message area begins -->                \r\n                <div #liveChatMsgDisplayArea fxFlex=\"87%\" fxFlex.lg=\"87%\" fxFlex.md=\"90%\" fxFlex.sm=\"85%\" fxFlex.xs=\"75%\" class=\"chat-area\">\r\n                  <app-chat-messages #chatMessages ></app-chat-messages>\r\n                </div>\r\n            <!-- chat-message area end -->\r\n            <!-- chat-sender begins -->\r\n             <div #liveChatSender class=\"chatSenderDiv\" fxFlex=\"6%\" fxFlex.lg=\"6%\" fxFlex.md=\"6%\" fxFlex.sm=\"5%\" fxFlex.xs=\"8%\">              \r\n              <app-chat-sender #appChatSender></app-chat-sender>\r\n             </div>            \r\n            <!-- chat-sender end-->\r\n          </div>\r\n        </div>\r\n\r\n<div class=\"overlay\" *ngIf=\"isDisable\"></div>\r\n<router-outlet></router-outlet>\r\n "

/***/ }),

/***/ "./src/app/dialog/dialog.component.css":
/*!*********************************************!*\
  !*** ./src/app/dialog/dialog.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".overlay {\r\n  position: fixed;\r\n  top: 0;\r\n  bottom: 0;\r\n  left: 0;\r\n  right: 0;\r\n  background-color: rgba(0, 0, 0, 0.5);\r\n  z-index: 999;\r\n}\r\n\r\n.dialog {\r\n  z-index: 1000;\r\n  position: absolute;\r\n  top: 35%;\r\n  width: 70%;\r\n  padding: 10px;\r\n  background-color: #c1e2f5;\r\n  border-radius: 6px;\r\n  font-family: Arial;\r\n  font-size: 10px;\r\n  margin-left: 15%;\r\n  margin-top: 8%;\r\n  box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);\r\n}\r\n\r\n@media (min-width: 768px) {\r\n  .dialog {\r\n    top: 25%;\r\n  }\r\n}\r\n\r\n.dialog__close-btn {\r\n  border: 0;\r\n  background: none;\r\n  color: #2d2d2d;\r\n  position: absolute;\r\n  top: 8px;\r\n  right: 8px;\r\n  font-size: 1.2em;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGlhbG9nL2RpYWxvZy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBZTtFQUNmLE1BQU07RUFDTixTQUFTO0VBQ1QsT0FBTztFQUNQLFFBQVE7RUFDUixvQ0FBb0M7RUFDcEMsWUFBWTtBQUNkOztBQUVBO0VBQ0UsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsVUFBVTtFQUNWLGFBQWE7RUFDYix5QkFBeUI7RUFDekIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFHZCw0Q0FBNEM7QUFDOUM7O0FBRUE7RUFDRTtJQUNFLFFBQVE7RUFDVjtBQUNGOztBQUVBO0VBQ0UsU0FBUztFQUNULGdCQUFnQjtFQUNoQixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixVQUFVO0VBQ1YsZ0JBQWdCO0FBQ2xCIiwiZmlsZSI6InNyYy9hcHAvZGlhbG9nL2RpYWxvZy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm92ZXJsYXkge1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICB0b3A6IDA7XHJcbiAgYm90dG9tOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgcmlnaHQ6IDA7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjUpO1xyXG4gIHotaW5kZXg6IDk5OTtcclxufVxyXG5cclxuLmRpYWxvZyB7XHJcbiAgei1pbmRleDogMTAwMDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiAzNSU7XHJcbiAgd2lkdGg6IDcwJTtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNjMWUyZjU7XHJcbiAgYm9yZGVyLXJhZGl1czogNnB4O1xyXG4gIGZvbnQtZmFtaWx5OiBBcmlhbDtcclxuICBmb250LXNpemU6IDEwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDE1JTtcclxuICBtYXJnaW4tdG9wOiA4JTtcclxuICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAycHggNXB4IDBweCByZ2JhKDAsMCwwLDAuNzUpO1xyXG4gIC1tb3otYm94LXNoYWRvdzogMHB4IDJweCA1cHggMHB4IHJnYmEoMCwwLDAsMC43NSk7XHJcbiAgYm94LXNoYWRvdzogMHB4IDJweCA1cHggMHB4IHJnYmEoMCwwLDAsMC43NSk7XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xyXG4gIC5kaWFsb2cge1xyXG4gICAgdG9wOiAyNSU7XHJcbiAgfVxyXG59XHJcblxyXG4uZGlhbG9nX19jbG9zZS1idG4ge1xyXG4gIGJvcmRlcjogMDtcclxuICBiYWNrZ3JvdW5kOiBub25lO1xyXG4gIGNvbG9yOiAjMmQyZDJkO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDhweDtcclxuICByaWdodDogOHB4O1xyXG4gIGZvbnQtc2l6ZTogMS4yZW07XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/dialog/dialog.component.ts":
/*!********************************************!*\
  !*** ./src/app/dialog/dialog.component.ts ***!
  \********************************************/
/*! exports provided: DialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DialogComponent", function() { return DialogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm2015/animations.js");



let DialogComponent = class DialogComponent {
    constructor() {
        this.closable = true;
        this.visibleChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    ngOnInit() { }
    close() {
        this.visible = false;
        this.visibleChange.emit(this.visible);
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], DialogComponent.prototype, "closable", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
], DialogComponent.prototype, "visible", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
], DialogComponent.prototype, "visibleChange", void 0);
DialogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dialog',
        template: __webpack_require__(/*! raw-loader!./dialog.component.html */ "./node_modules/raw-loader/index.js!./src/app/dialog/dialog.component.html"),
        animations: [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["trigger"])('dialog', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["transition"])('void => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ transform: 'scale3d(.3, .3, .3)' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["animate"])(100)
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["transition"])('* => void', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["animate"])(100, Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ transform: 'scale3d(.0, .0, .0)' }))
                ])
            ])
        ],
        styles: [__webpack_require__(/*! ./dialog.component.css */ "./src/app/dialog/dialog.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], DialogComponent);



/***/ }),

/***/ "./src/app/ffq-livechat.module.ts":
/*!****************************************!*\
  !*** ./src/app/ffq-livechat.module.ts ***!
  \****************************************/
/*! exports provided: FfqLivechatModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FfqLivechatModule", function() { return FfqLivechatModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _livechat_home_livechat_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./livechat-home/livechat-home.component */ "./src/app/livechat-home/livechat-home.component.ts");
/* harmony import */ var _livechat_home_chat_header_chat_header_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./livechat-home/chat-header/chat-header.component */ "./src/app/livechat-home/chat-header/chat-header.component.ts");
/* harmony import */ var _livechat_home_chat_messages_chat_messages_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./livechat-home/chat-messages/chat-messages.component */ "./src/app/livechat-home/chat-messages/chat-messages.component.ts");
/* harmony import */ var _livechat_home_chat_sender_chat_sender_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./livechat-home/chat-sender/chat-sender.component */ "./src/app/livechat-home/chat-sender/chat-sender.component.ts");
/* harmony import */ var _shared_modules_angular_material_angular_material_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./shared/modules/angular-material/angular-material.module */ "./src/app/shared/modules/angular-material/angular-material.module.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _dialog_dialog_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./dialog/dialog.component */ "./src/app/dialog/dialog.component.ts");












const routes = [
    { path: '', component: _livechat_home_livechat_home_component__WEBPACK_IMPORTED_MODULE_3__["LivechatHomeComponent"] }
];
let FfqLivechatModule = class FfqLivechatModule {
};
FfqLivechatModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _livechat_home_livechat_home_component__WEBPACK_IMPORTED_MODULE_3__["LivechatHomeComponent"],
            _livechat_home_chat_header_chat_header_component__WEBPACK_IMPORTED_MODULE_4__["ChatHeaderComponent"],
            _livechat_home_chat_messages_chat_messages_component__WEBPACK_IMPORTED_MODULE_5__["ChatMessagesComponent"],
            _livechat_home_chat_sender_chat_sender_component__WEBPACK_IMPORTED_MODULE_6__["ChatSenderComponent"],
            _dialog_dialog_component__WEBPACK_IMPORTED_MODULE_11__["DialogComponent"]
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterModule"].forChild(routes),
            _shared_modules_angular_material_angular_material_module__WEBPACK_IMPORTED_MODULE_7__["AngularMaterialModule"],
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_9__["FlexLayoutModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormsModule"]
        ]
    })
], FfqLivechatModule);



/***/ }),

/***/ "./src/app/livechat-home/chat-header/chat-header.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/livechat-home/chat-header/chat-header.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sub-header{\r\n\tcolor: rgba(255,255,255,0.5);\r\n    /* font-family: \"Slate Pro\"; */\r\n    font-size: 12px;\r\n    line-height: 14px;\r\n    font-weight: normal !important;\r\n}\r\n\r\n.main-header {\r\n\tcolor: #FFFFFF;\r\n    font-size: 16px;\r\n    font-weight: bold;\r\n    line-height: 19px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGl2ZWNoYXQtaG9tZS9jaGF0LWhlYWRlci9jaGF0LWhlYWRlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0NBQ0MsNEJBQTRCO0lBQ3pCLDhCQUE4QjtJQUM5QixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLDhCQUE4QjtBQUNsQzs7QUFFQTtDQUNDLGNBQWM7SUFDWCxlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGlCQUFpQjtBQUNyQiIsImZpbGUiOiJzcmMvYXBwL2xpdmVjaGF0LWhvbWUvY2hhdC1oZWFkZXIvY2hhdC1oZWFkZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zdWItaGVhZGVye1xyXG5cdGNvbG9yOiByZ2JhKDI1NSwyNTUsMjU1LDAuNSk7XHJcbiAgICAvKiBmb250LWZhbWlseTogXCJTbGF0ZSBQcm9cIjsgKi9cclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxNHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubWFpbi1oZWFkZXIge1xyXG5cdGNvbG9yOiAjRkZGRkZGO1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBsaW5lLWhlaWdodDogMTlweDtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/livechat-home/chat-header/chat-header.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/livechat-home/chat-header/chat-header.component.ts ***!
  \********************************************************************/
/*! exports provided: ChatHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatHeaderComponent", function() { return ChatHeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ChatHeaderComponent = class ChatHeaderComponent {
    constructor() { }
    ngOnInit() {
    }
};
ChatHeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-chat-header',
        template: __webpack_require__(/*! raw-loader!./chat-header.component.html */ "./node_modules/raw-loader/index.js!./src/app/livechat-home/chat-header/chat-header.component.html"),
        styles: [__webpack_require__(/*! ./chat-header.component.css */ "./src/app/livechat-home/chat-header/chat-header.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], ChatHeaderComponent);



/***/ }),

/***/ "./src/app/livechat-home/chat-messages/chat-messages.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/livechat-home/chat-messages/chat-messages.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".outgoingMessage {\r\n    word-wrap: break-word;\r\n    text-align: left!important;\r\n    max-width: 75%;\r\n    align-self: flex-end;\r\n    color: #FFFFFF;\r\n    /*padding: 10px !important;*/\r\n    border-radius: 5px; \r\n    background-color: #158FEF;\r\n    white-space: pre-wrap !important;\r\n    overflow-wrap: break-word;\r\n    padding-right: 15px !important;\r\n    padding-left: 15px !important;\r\n}\r\n\r\n.incomingMessage {\r\n    word-wrap: break-word;\r\n    text-align: left;\r\n    max-width: 75%;\r\n    align-self: flex-start;\r\n    color: #000000;\r\n    /*padding: 10px !important;*/\r\n    border-radius: 5px;\r\n    background-color: #F7F7F7;\r\n    white-space: pre-wrap !important;\r\n    overflow-wrap: break-word;\r\n\t  padding-left: 14px !important;\r\n    padding-right: 14px !important;\r\n}\r\n\r\n.outgoingMsgUserName{\r\n  color: red;\r\n  font-weight: bold !important;\r\n  font-family: Arial,Helvetica,sans-serif;\r\n  font-size: 13px !important;\r\n}\r\n\r\n.incomingMsgUserName{\r\n  color: #0073cf;\r\n  font-weight: bold !important;\r\n  font-family: Arial,Helvetica,sans-serif;\r\n  font-size: 13px !important;\r\n}\r\n\r\n.message {\r\n    font-family: Arial,Helvetica,sans-serif;\r\n    font-size: 14px !important;\r\n    line-height: 16px;\r\n    padding-top: 13px !important;\r\n    padding-bottom: 13px !important;\r\n}\r\n\r\n.message-out {\r\n    font-family: Arial,Helvetica,sans-serif;\r\n    font-size: 15px !important;\r\n    line-height: 16px;\r\n    padding-top: 13px !important;\r\n    padding-bottom: 13px !important;\r\n}\r\n\r\n.userNameAndTimeStamp{\r\n  color: #B6B6B6;\r\n  font-family: Arial,Helvetica,sans-serif;\r\n  line-height: 14px;\r\n  font-size: 12px !important;\r\n}\r\n\r\n.mat-list-base .mat-list-item.mat-2-line, .mat-list-base .mat-list-option.mat-2-line {\r\n    height: -webkit-fit-content !important;\r\n    height: -moz-fit-content !important;\r\n    height: fit-content !important;\r\n}\r\n\r\n.timestampWrapper{\r\n  margin-bottom: 8px !important;\r\n}\r\n\r\n.welcomeMsg{\r\n    margin-top: 10px;\r\n    margin-bottom: 10px;\r\n}\r\n\r\n.mat-list-base .mat-list-item .mat-line {\r\n  text-overflow: inherit;\r\n}\r\n\r\n.typingStatus{\r\n  color: grey;\r\n  font-size: 11px !important;\r\n  font-family: Arial;\r\n  font-style: italic;\r\n\ttext-align: left;\r\n  max-width: 75%;\r\n  align-self: flex-start;\r\n\tpadding-left: 14px !important;\r\n  padding-right: 14px !important;\r\n}\r\n\r\n/*IE specific fix - incoming outgoing messages overlapping for long messages*/\r\n\r\n@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {\r\n  .mat-list-base .mat-list-item.mat-2-line, .mat-list-base .mat-list-option.mat-2-line {\r\n    height: auto !important;\r\n  }\r\n}\r\n\r\n/*IE Edge specific fix - incoming outgoing messages overlapping for long messages*/\r\n\r\n@supports (-ms-ime-align:auto) and (-webkit-text-stroke:initial) {\r\n  .mat-list-base .mat-list-item.mat-2-line, .mat-list-base .mat-list-option.mat-2-line {\r\n    height: auto !important;\r\n  }\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGl2ZWNoYXQtaG9tZS9jaGF0LW1lc3NhZ2VzL2NoYXQtbWVzc2FnZXMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLHFCQUFxQjtJQUNyQiwwQkFBMEI7SUFDMUIsY0FBYztJQUNkLG9CQUFvQjtJQUNwQixjQUFjO0lBQ2QsNEJBQTRCO0lBQzVCLGtCQUFrQjtJQUNsQix5QkFBeUI7SUFDekIsZ0NBQWdDO0lBQ2hDLHlCQUF5QjtJQUN6Qiw4QkFBOEI7SUFDOUIsNkJBQTZCO0FBQ2pDOztBQUVBO0lBQ0kscUJBQXFCO0lBQ3JCLGdCQUFnQjtJQUNoQixjQUFjO0lBQ2Qsc0JBQXNCO0lBQ3RCLGNBQWM7SUFDZCw0QkFBNEI7SUFDNUIsa0JBQWtCO0lBQ2xCLHlCQUF5QjtJQUN6QixnQ0FBZ0M7SUFDaEMseUJBQXlCO0dBQzFCLDZCQUE2QjtJQUM1Qiw4QkFBOEI7QUFDbEM7O0FBRUE7RUFDRSxVQUFVO0VBQ1YsNEJBQTRCO0VBQzVCLHVDQUF1QztFQUN2QywwQkFBMEI7QUFDNUI7O0FBRUE7RUFDRSxjQUFjO0VBQ2QsNEJBQTRCO0VBQzVCLHVDQUF1QztFQUN2QywwQkFBMEI7QUFDNUI7O0FBR0E7SUFDSSx1Q0FBdUM7SUFDdkMsMEJBQTBCO0lBQzFCLGlCQUFpQjtJQUNqQiw0QkFBNEI7SUFDNUIsK0JBQStCO0FBQ25DOztBQUVBO0lBQ0ksdUNBQXVDO0lBQ3ZDLDBCQUEwQjtJQUMxQixpQkFBaUI7SUFDakIsNEJBQTRCO0lBQzVCLCtCQUErQjtBQUNuQzs7QUFHQTtFQUNFLGNBQWM7RUFDZCx1Q0FBdUM7RUFDdkMsaUJBQWlCO0VBQ2pCLDBCQUEwQjtBQUM1Qjs7QUFFQTtJQUNJLHNDQUE4QjtJQUE5QixtQ0FBOEI7SUFBOUIsOEJBQThCO0FBQ2xDOztBQUVBO0VBQ0UsNkJBQTZCO0FBQy9COztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLG1CQUFtQjtBQUN2Qjs7QUFFQTtFQUNFLHNCQUFzQjtBQUN4Qjs7QUFFQTtFQUNFLFdBQVc7RUFDWCwwQkFBMEI7RUFDMUIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtDQUNuQixnQkFBZ0I7RUFDZixjQUFjO0VBQ2Qsc0JBQXNCO0NBQ3ZCLDZCQUE2QjtFQUM1Qiw4QkFBOEI7QUFDaEM7O0FBRUEsNkVBQTZFOztBQUM3RTtFQUNFO0lBQ0UsdUJBQXVCO0VBQ3pCO0FBQ0Y7O0FBRUEsa0ZBQWtGOztBQUNsRjtFQUNFO0lBQ0UsdUJBQXVCO0VBQ3pCO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9saXZlY2hhdC1ob21lL2NoYXQtbWVzc2FnZXMvY2hhdC1tZXNzYWdlcy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm91dGdvaW5nTWVzc2FnZSB7XHJcbiAgICB3b3JkLXdyYXA6IGJyZWFrLXdvcmQ7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0IWltcG9ydGFudDtcclxuICAgIG1heC13aWR0aDogNzUlO1xyXG4gICAgYWxpZ24tc2VsZjogZmxleC1lbmQ7XHJcbiAgICBjb2xvcjogI0ZGRkZGRjtcclxuICAgIC8qcGFkZGluZzogMTBweCAhaW1wb3J0YW50OyovXHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7IFxyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzE1OEZFRjtcclxuICAgIHdoaXRlLXNwYWNlOiBwcmUtd3JhcCAhaW1wb3J0YW50O1xyXG4gICAgb3ZlcmZsb3ctd3JhcDogYnJlYWstd29yZDtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDE1cHggIWltcG9ydGFudDtcclxuICAgIHBhZGRpbmctbGVmdDogMTVweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uaW5jb21pbmdNZXNzYWdlIHtcclxuICAgIHdvcmQtd3JhcDogYnJlYWstd29yZDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICBtYXgtd2lkdGg6IDc1JTtcclxuICAgIGFsaWduLXNlbGY6IGZsZXgtc3RhcnQ7XHJcbiAgICBjb2xvcjogIzAwMDAwMDtcclxuICAgIC8qcGFkZGluZzogMTBweCAhaW1wb3J0YW50OyovXHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjdGN0Y3O1xyXG4gICAgd2hpdGUtc3BhY2U6IHByZS13cmFwICFpbXBvcnRhbnQ7XHJcbiAgICBvdmVyZmxvdy13cmFwOiBicmVhay13b3JkO1xyXG5cdCAgcGFkZGluZy1sZWZ0OiAxNHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5vdXRnb2luZ01zZ1VzZXJOYW1le1xyXG4gIGNvbG9yOiByZWQ7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQgIWltcG9ydGFudDtcclxuICBmb250LWZhbWlseTogQXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7XHJcbiAgZm9udC1zaXplOiAxM3B4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5pbmNvbWluZ01zZ1VzZXJOYW1le1xyXG4gIGNvbG9yOiAjMDA3M2NmO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkICFpbXBvcnRhbnQ7XHJcbiAgZm9udC1mYW1pbHk6IEFyaWFsLEhlbHZldGljYSxzYW5zLXNlcmlmO1xyXG4gIGZvbnQtc2l6ZTogMTNweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5cclxuLm1lc3NhZ2Uge1xyXG4gICAgZm9udC1mYW1pbHk6IEFyaWFsLEhlbHZldGljYSxzYW5zLXNlcmlmO1xyXG4gICAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBsaW5lLWhlaWdodDogMTZweDtcclxuICAgIHBhZGRpbmctdG9wOiAxM3B4ICFpbXBvcnRhbnQ7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTNweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubWVzc2FnZS1vdXQge1xyXG4gICAgZm9udC1mYW1pbHk6IEFyaWFsLEhlbHZldGljYSxzYW5zLXNlcmlmO1xyXG4gICAgZm9udC1zaXplOiAxNXB4ICFpbXBvcnRhbnQ7XHJcbiAgICBsaW5lLWhlaWdodDogMTZweDtcclxuICAgIHBhZGRpbmctdG9wOiAxM3B4ICFpbXBvcnRhbnQ7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTNweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5cclxuLnVzZXJOYW1lQW5kVGltZVN0YW1we1xyXG4gIGNvbG9yOiAjQjZCNkI2O1xyXG4gIGZvbnQtZmFtaWx5OiBBcmlhbCxIZWx2ZXRpY2Esc2Fucy1zZXJpZjtcclxuICBsaW5lLWhlaWdodDogMTRweDtcclxuICBmb250LXNpemU6IDEycHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1hdC1saXN0LWJhc2UgLm1hdC1saXN0LWl0ZW0ubWF0LTItbGluZSwgLm1hdC1saXN0LWJhc2UgLm1hdC1saXN0LW9wdGlvbi5tYXQtMi1saW5lIHtcclxuICAgIGhlaWdodDogZml0LWNvbnRlbnQgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnRpbWVzdGFtcFdyYXBwZXJ7XHJcbiAgbWFyZ2luLWJvdHRvbTogOHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi53ZWxjb21lTXNne1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbn1cclxuXHJcbi5tYXQtbGlzdC1iYXNlIC5tYXQtbGlzdC1pdGVtIC5tYXQtbGluZSB7XHJcbiAgdGV4dC1vdmVyZmxvdzogaW5oZXJpdDtcclxufVxyXG5cclxuLnR5cGluZ1N0YXR1c3tcclxuICBjb2xvcjogZ3JleTtcclxuICBmb250LXNpemU6IDExcHggIWltcG9ydGFudDtcclxuICBmb250LWZhbWlseTogQXJpYWw7XHJcbiAgZm9udC1zdHlsZTogaXRhbGljO1xyXG5cdHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgbWF4LXdpZHRoOiA3NSU7XHJcbiAgYWxpZ24tc2VsZjogZmxleC1zdGFydDtcclxuXHRwYWRkaW5nLWxlZnQ6IDE0cHggIWltcG9ydGFudDtcclxuICBwYWRkaW5nLXJpZ2h0OiAxNHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8qSUUgc3BlY2lmaWMgZml4IC0gaW5jb21pbmcgb3V0Z29pbmcgbWVzc2FnZXMgb3ZlcmxhcHBpbmcgZm9yIGxvbmcgbWVzc2FnZXMqL1xyXG5AbWVkaWEgYWxsIGFuZCAoLW1zLWhpZ2gtY29udHJhc3Q6IG5vbmUpLCAoLW1zLWhpZ2gtY29udHJhc3Q6IGFjdGl2ZSkge1xyXG4gIC5tYXQtbGlzdC1iYXNlIC5tYXQtbGlzdC1pdGVtLm1hdC0yLWxpbmUsIC5tYXQtbGlzdC1iYXNlIC5tYXQtbGlzdC1vcHRpb24ubWF0LTItbGluZSB7XHJcbiAgICBoZWlnaHQ6IGF1dG8gIWltcG9ydGFudDtcclxuICB9XHJcbn1cclxuXHJcbi8qSUUgRWRnZSBzcGVjaWZpYyBmaXggLSBpbmNvbWluZyBvdXRnb2luZyBtZXNzYWdlcyBvdmVybGFwcGluZyBmb3IgbG9uZyBtZXNzYWdlcyovXHJcbkBzdXBwb3J0cyAoLW1zLWltZS1hbGlnbjphdXRvKSBhbmQgKC13ZWJraXQtdGV4dC1zdHJva2U6aW5pdGlhbCkge1xyXG4gIC5tYXQtbGlzdC1iYXNlIC5tYXQtbGlzdC1pdGVtLm1hdC0yLWxpbmUsIC5tYXQtbGlzdC1iYXNlIC5tYXQtbGlzdC1vcHRpb24ubWF0LTItbGluZSB7XHJcbiAgICBoZWlnaHQ6IGF1dG8gIWltcG9ydGFudDtcclxuICB9XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/livechat-home/chat-messages/chat-messages.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/livechat-home/chat-messages/chat-messages.component.ts ***!
  \************************************************************************/
/*! exports provided: ChatMessagesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatMessagesComponent", function() { return ChatMessagesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_message_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/message.service */ "./src/app/services/message.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _services_chat_socket_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/chat-socket.service */ "./src/app/services/chat-socket.service.ts");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/shared.service */ "./src/app/services/shared.service.ts");
/* harmony import */ var _services_context_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/context.service */ "./src/app/services/context.service.ts");
/* harmony import */ var _store_app_selectors__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../store/app.selectors */ "./src/store/app.selectors.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm2015/store.js");









let ChatMessagesComponent = class ChatMessagesComponent {
    /**
     * [constructor description]
     * @param  [type] private messageService: MessageService           [description]
     * @param  [type] private datePipe:       DatePipe                 [description]
     * @param  [type] private socketService:  ChatSocketService		 [description]
     * @return [type]         [description]
     */
    constructor(messageService, datePipe, socketService, sharedService, ref, contextService, store) {
        this.messageService = messageService;
        this.datePipe = datePipe;
        this.socketService = socketService;
        this.sharedService = sharedService;
        this.ref = ref;
        this.contextService = contextService;
        this.store = store;
        this.isChrome = false;
        this.messages = [];
        this.showLoad = true;
        this.csrTransferWaitingMsgCount = 0;
        // user inactivity changes
        this.idleUserTimeoutWarning = 180;
        this.idleUserTimeoutBookend = 360;
        this.warnMsgCount = 0;
        this.idleUserTimeoutWarningMessage = 'I’m ready for your question.';
        this.idleUserTimeoutBookendMessage = '';
        this.timeoutIntervals = [50, 120, 180, 180];
        this.modalEventMessage = '';
        this.dialogMessage = '';
        this.eventMessage = '';
        this.context$ = this.store.select(_store_app_selectors__WEBPACK_IMPORTED_MODULE_7__["getContext"]);
        this.agentTyping = false;
        this.agentTypingMessage = 'The Agent is typing.';
        this.context$.subscribe(ctx => {
            this.context = ((ctx.liveChatContext === null) ? JSON.parse(sessionStorage.getItem('context')) : ctx);
        });
        const appProperties = this.context.liveChatContext;
        this.quoteNumber = appProperties.quote_number;
        // detect browser for sound IE issue fix
        // this.isChrome = !!window.chrome && !!window.chrome.webstore;
        // subscribe to home component messages
        this.subscription = this.messageService.getMessage().subscribe(messageList => {
            this.messages = messageList;
            this.ref.detectChanges();
        });
        this.subscription1 = this.sharedService.getSendMessage().subscribe(msgs => {
            this.currentMessage = msgs;
            // if(this.currentMessage.messageType == 'warningMessage' || this.currentMessage.messageType =='bookendWarningMessage'){
            // 	this.addWarningMessage(messageObj);
            // }else
            if (this.currentMessage.messageType === 'outgoingMsg') {
                this.stopNoUserInputTimeOuts();
            }
        });
        this.subscription = this.sharedService.getAgentTypingStatus().subscribe(status => {
            this.agentTyping = status;
        });
        this.sharedService.getModalParams().subscribe(messageObj => {
            console.log('messageObj.flag ' + messageObj.flag);
            // hide the loader and show error messgae in overlay
            this.showLoad = false;
            console.log('Event Message' + messageObj.message);
            this.eventMessage = this.deriveActualConnectionTimeout(messageObj.message);
            if ('ConnectionLostDuringCSRChat' === this.eventMessage) {
                console.log('Applying 10 Second delay on losing connectivity during CSR chat before showing dialoge box');
                setTimeout(() => {
                    this.triggerMessage(this.eventMessage);
                }, 10000);
            }
            else {
                this.triggerMessage(this.eventMessage);
            }
        });
        this.subscription1 = this.sharedService.getDisableMessage().subscribe(msgs => {
            console.log('Inside DisableMessage and stop InActivity Timer and  csr wait timer');
            this.agentTyping = false;
            this.stopNoUserInputTimeOuts();
            this.stopCSRTransferCounter();
            this.emitPreDisconnectEvent();
        });
        this.subscription = this.sharedService.getCsrTransferStatus().subscribe(status => {
            this.manageCSRTimeOutMessages(status);
        });
    }
    manageCSRTimeOutMessages(status) {
        if ('ChatRequestSuccess' === status) {
            this.enableCSRApologyMessages();
        }
        else if ('ChatEstablished' === status) {
            //   this.chatData.salesForceDetails.isCsrAcceptRequest = true;
            //   this.isCsrAcceptRequest = true;
            this.stopCSRTransferCounter();
        }
    }
    enableCSRApologyMessages() {
        this.timeoutIntervalsCount = this.timeoutIntervals.length;
        this.timeoutCheckCount = this.timeoutIntervalsCount - 1;
        if (this.csrTransferWaitingMsgCount < this.timeoutCheckCount) { // this.csrIdleTimeCount instead of 5 changes later
            this.startTimerForWaitingMessage(this.timeoutIntervals[this.csrTransferWaitingMsgCount]);
            this.csrTransferWaitingMsgCount++;
        }
        else { // this.csrIdleTimeCount
            // this.sendCSRFinalMsgToChatHistory(this.csrFinalMessage);
            this.startTimerForFinalWaitMessage(this.timeoutIntervals[this.csrTransferWaitingMsgCount]);
        }
    }
    startTimerForFinalWaitMessage(timeout) {
        this.csrWaitingTimer = setTimeout(() => {
            this.messageService.updateMessage({
                message: '',
                messageType: 'errorMsg',
                userType: '',
                timeStamp: this.datePipe.transform(new Date(), 'shortTime'),
                error: false,
                userName: ''
            });
            this.sharedService.setDisableMessage('Connection Issue');
            this.emitPreDisconnectEvent();
        }, this.remainingCsrWaitingTime ? this.remainingCsrWaitingTime : timeout * 1000);
    }
    startTimerForWaitingMessage(timeout) {
        this.initialCsrWaitingTime = Date.now();
        this.csrWaitingTimer = setTimeout(() => {
            this.messageService.updateMessage({
                message: 'So sorry for the delay — we’ll be with you as soon as possible.',
                messageType: 'errorMsg',
                userType: '',
                timeStamp: this.datePipe.transform(new Date(), 'shortTime'),
                error: false,
                userName: ''
            });
            this.enableCSRApologyMessages();
        }, this.remainingCsrWaitingTime ? this.remainingCsrWaitingTime : timeout * 1000);
        // this.csrWaitingTimeoutInterval change later change to 50 secs wait time
    }
    stopCSRTransferCounter() {
        clearTimeout(this.csrWaitingTimer);
        this.remainingCsrWaitingTime = undefined;
    }
    ngOnInit() {
        this.idleUserTimeoutWarningInterval = this.idleUserTimeoutWarning;
        this.idleUserTimeoutBookendInterval = this.idleUserTimeoutBookend;
        this.startIdleUserTimeoutWarning();
        this.startIdleUserTimeoutBookend();
    }
    triggerMessage(eventMessage) {
        console.log('Inside trigger message function');
        this.modalEventMessage = eventMessage;
        switch (this.modalEventMessage) {
            case 'SocketConnectionEstablished':
                this.showPopUp = false;
                break;
            case 'ConnectionLost':
                // this.showPopUp = true;
                // this.dialogMessage = 'Connection lost: Please check your local connection.';
                console.log('Starting populate message');
                this.populateWaitingMessage('Connection lost: Please check your local connection.', 'warningMessage');
                break;
            case 'ConnectionLostDuringCSRChat':
                console.log('Starting populate message');
                this.populateWaitingMessage('We\'re reconnecting your chat. Please wait.', 'warningMessage');
                break;
        }
    }
    deriveActualConnectionTimeout(eventMessage) {
        console.log('Event message: ' + eventMessage);
        if ('ConnectionLost' === eventMessage) {
            return 'ConnectionLostDuringCSRChat';
        }
        else {
            return eventMessage;
        }
    }
    populateWaitingMessage(warnMessage, warnMessageType) {
        const waitingMessage = {
            userType: 'SRM',
            userName: '',
            message: warnMessage,
            messageType: warnMessageType,
            timeStamp: this.sharedService.getDatetime(),
            error: false
        };
        this.messageService.updateMessage(waitingMessage);
        // this.addMessageToDisplayWindow(waitingMessage);
    }
    startIdleUserTimeoutWarning() {
        console.log('inside inactivity method 1');
        this.initialWarningTime = Date.now();
        this.idleUserTimeoutWarningTimer = setTimeout(() => {
            console.log('inside inactivity method 2');
            if (!this.warnMsgCount) {
                this.populateWaitingMessage(this.idleUserTimeoutWarningMessage, 'warningMessage');
                this.warnMsgCount = 1;
            }
        }, this.remainingWarningTime ? this.remainingWarningTime : this.idleUserTimeoutWarningInterval * 1000);
    }
    startIdleUserTimeoutBookend() {
        this.initialBookendTime = Date.now();
        this.idleUserTimeoutBookendTimer = setTimeout(() => {
            if (this.warnMsgCount) {
                this.populateWaitingMessage(this.idleUserTimeoutBookendMessage, 'bookendWarningMessage');
                this.stopNoUserInputTimeOuts();
                this.sharedService.setIsChatEnded(true);
                this.emitPreDisconnectEvent();
            }
        }, this.remainingBookendTime ? this.remainingBookendTime : this.idleUserTimeoutBookendInterval * 1000);
    }
    stopNoUserInputTimeOuts() {
        clearTimeout(this.idleUserTimeoutWarningTimer);
        clearTimeout(this.idleUserTimeoutBookendTimer);
        this.remainingWarningTime = undefined;
        this.remainingBookendTime = undefined;
    }
    pauseNoUserInputTimeOuts() {
        this.stopNoUserInputTimeOuts();
        this.remainingWarningTime = this.idleUserTimeoutWarning * 1000;
        this.remainingBookendTime = this.idleUserTimeoutBookend * 1000;
    }
    /**
     * [resumeNoUserInputTimeOuts description]
     * @return [type] [description]
     */
    resumeNoUserInputTimeOuts() {
        if (this.remainingWarningTime > 0 && !this.warnMsgCount) {
            this.startIdleUserTimeoutWarning();
            this.startIdleUserTimeoutBookend();
        }
        if (this.remainingBookendTime > 0 && this.warnMsgCount) {
            this.startIdleUserTimeoutBookend();
        }
    }
    /**
     * [emitPreDisconnectEvent description]
     * @return [type] [description]
     */
    emitPreDisconnectEvent() {
        if (this.sharedService.getIsChatEnded()) {
            const endChatData = {
                messageType: 'chatEnded'
            };
            // this.socketService.sendClientSideMsgToAuditLogs(endChatData, false);
            this.socketService.endLiveChat(this.quoteNumber);
        }
    }
};
ChatMessagesComponent.ctorParameters = () => [
    { type: _services_message_service__WEBPACK_IMPORTED_MODULE_2__["MessageService"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_3__["DatePipe"] },
    { type: _services_chat_socket_service__WEBPACK_IMPORTED_MODULE_4__["ChatSocketService"] },
    { type: _services_shared_service__WEBPACK_IMPORTED_MODULE_5__["SharedService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
    { type: _services_context_service__WEBPACK_IMPORTED_MODULE_6__["ContextService"] },
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_8__["Store"] }
];
ChatMessagesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-chat-messages',
        template: __webpack_require__(/*! raw-loader!./chat-messages.component.html */ "./node_modules/raw-loader/index.js!./src/app/livechat-home/chat-messages/chat-messages.component.html"),
        providers: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["DatePipe"]],
        styles: [__webpack_require__(/*! ./chat-messages.component.css */ "./src/app/livechat-home/chat-messages/chat-messages.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_message_service__WEBPACK_IMPORTED_MODULE_2__["MessageService"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["DatePipe"],
        _services_chat_socket_service__WEBPACK_IMPORTED_MODULE_4__["ChatSocketService"], _services_shared_service__WEBPACK_IMPORTED_MODULE_5__["SharedService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
        _services_context_service__WEBPACK_IMPORTED_MODULE_6__["ContextService"], _ngrx_store__WEBPACK_IMPORTED_MODULE_8__["Store"]])
], ChatMessagesComponent);



/***/ }),

/***/ "./src/app/livechat-home/chat-sender/chat-sender.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/livechat-home/chat-sender/chat-sender.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".chatMessageWrapper{\r\n    background-color: white;\r\n  /*  height: 30px;*/\r\n    width: calc(100% - 65px);\r\n\tpadding-top: 5px;\r\n}\r\n.chatMessageWrapperGrey{\r\n  background-color: F7F7F7;\r\n/*  height: 30px;*/\r\n  width: 110%;\r\npadding-top: 5px;\r\n}\r\n.chatMessagGreyOut{\r\n  background-color: #F7F7F7;\r\n\r\n}\r\n.btnSendMessage{\r\n    height: 39px;\r\n    width: 58px;\r\n    background: linear-gradient(to bottom, #f7f7f7 0%, #f7f7f7 50%, #e7e7e7 50%, #e7e7e7 100%);\r\n    color: #0073cf;\r\n    font-weight: 700;\r\n    font-family: Arial;\r\n    font-size: 13px;\r\n    cursor: pointer;\r\n    border: none;\r\n    border-radius: 0px;\r\n    bottom: 28px;\r\n    float: right;\r\n}\r\n.btnSendMessage:focus {\r\n    outline: none;\r\n}\r\n.txtAreaType {\r\n  \twidth: 100%;\r\n    white-space: pre-wrap;\r\n    overflow-y: auto;\r\n    overflow-x: hidden;\r\n    background-color: #fff;\r\n    line-height: 13px;\r\n    -webkit-line-break: after-white-space;\r\n    word-wrap: break-word;\r\n    outline-style: none;\r\n    display: inline-block;\r\n    font-size: 13px; \r\n    padding-top: 13px;\r\n    margin: 0px;\r\n    font-family: Arial, Helvetica,sans-serif;\r\n    border: none;\r\n    resize: none;\r\n    padding-left: 20px;\r\n    height: 38px;\r\n  }\r\n.txtAreaTypeGrey {\r\n      width: 100%;\r\n      white-space: pre-wrap;\r\n      overflow-y: auto;\r\n      overflow-x: hidden;\r\n      background-color:#F7F7F7;\r\n      line-height: 13px;\r\n      -webkit-line-break: after-white-space;\r\n      word-wrap: break-word;\r\n      outline-style: none;\r\n      display: inline-block;\r\n      font-size: 13px; \r\n      padding-top: 13px;\r\n      margin: 0px;\r\n      font-family: Arial, Helvetica,sans-serif;\r\n      border: none;\r\n      resize: none;\r\n      padding-left: 20px;\r\n      height: 38px;\r\n    }\r\n.chatMessageWrapper button {\r\n    width: 36px;\r\n    height: 36px;\r\n    padding: 0;\r\n    margin: 0!important;\r\n    border: none;\r\n    background: url(/../../../assets/icons/btn_send.png) no-repeat 50%;\r\n    position: absolute;\r\n    padding-left: 80px;\r\n   /* right: 30px;\r\n    bottom: 6px;*/\r\n   /* background: linear-gradient(180deg, #158FEF 0%, #0073CF 100%);*/\r\n}\r\n.chatMessageWrapper button:focus {\r\n    outline: none !important;\r\n}\r\n#messageToSend::-webkit-input-placeholder {\r\n    font-family: inherit;\r\n    color: #585858;\r\n    font-size: 13px;\r\n  }\r\n#messageToSend:-ms-input-placeholder {\r\n    font-family: inherit;\r\n    color: #585858;\r\n    font-size: 13px;\r\n  }\r\n#messageToSend::-ms-input-placeholder {\r\n    font-family: inherit;\r\n    color: #585858;\r\n    font-size: 13px;\r\n  }\r\n#messageToSend:-moz-placeholder {\r\n    font-family: inherit;\r\n    color: #585858;\r\n    font-size: 13px;\r\n  }\r\n#messageToSend::-moz-placeholder {\r\n    font-family: inherit;\r\n    color: #585858;\r\n    font-size: 13px;\r\n  }\r\n  \r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGl2ZWNoYXQtaG9tZS9jaGF0LXNlbmRlci9jaGF0LXNlbmRlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksdUJBQXVCO0VBQ3pCLGtCQUFrQjtJQUNoQix3QkFBd0I7Q0FDM0IsZ0JBQWdCO0FBQ2pCO0FBQ0E7RUFDRSx3QkFBd0I7QUFDMUIsa0JBQWtCO0VBQ2hCLFdBQVc7QUFDYixnQkFBZ0I7QUFDaEI7QUFDQTtFQUNFLHlCQUF5Qjs7QUFFM0I7QUFDQTtJQUNJLFlBQVk7SUFDWixXQUFXO0lBQ1gsMEZBQTBGO0lBQzFGLGNBQWM7SUFDZCxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixlQUFlO0lBQ2YsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osWUFBWTtBQUNoQjtBQUNBO0lBQ0ksYUFBYTtBQUNqQjtBQUVBO0dBQ0csV0FBVztJQUNWLHFCQUFxQjtJQUNyQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLHNCQUFzQjtJQUN0QixpQkFBaUI7SUFDakIscUNBQXFDO0lBQ3JDLHFCQUFxQjtJQUNyQixtQkFBbUI7SUFDbkIscUJBQXFCO0lBQ3JCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsV0FBVztJQUNYLHdDQUF3QztJQUN4QyxZQUFZO0lBQ1osWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixZQUFZO0VBQ2Q7QUFFQTtNQUNJLFdBQVc7TUFDWCxxQkFBcUI7TUFDckIsZ0JBQWdCO01BQ2hCLGtCQUFrQjtNQUNsQix3QkFBd0I7TUFDeEIsaUJBQWlCO01BQ2pCLHFDQUFxQztNQUNyQyxxQkFBcUI7TUFDckIsbUJBQW1CO01BQ25CLHFCQUFxQjtNQUNyQixlQUFlO01BQ2YsaUJBQWlCO01BQ2pCLFdBQVc7TUFDWCx3Q0FBd0M7TUFDeEMsWUFBWTtNQUNaLFlBQVk7TUFDWixrQkFBa0I7TUFDbEIsWUFBWTtJQUNkO0FBRUo7SUFDSSxXQUFXO0lBQ1gsWUFBWTtJQUNaLFVBQVU7SUFDVixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLGtFQUFrRTtJQUNsRSxrQkFBa0I7SUFDbEIsa0JBQWtCO0dBQ25CO2lCQUNjO0dBQ2Qsa0VBQWtFO0FBQ3JFO0FBRUE7SUFDSSx3QkFBd0I7QUFDNUI7QUFFQTtJQUNJLG9CQUFvQjtJQUNwQixjQUFjO0lBQ2QsZUFBZTtFQUNqQjtBQUVBO0lBQ0Usb0JBQW9CO0lBQ3BCLGNBQWM7SUFDZCxlQUFlO0VBQ2pCO0FBRUE7SUFDRSxvQkFBb0I7SUFDcEIsY0FBYztJQUNkLGVBQWU7RUFDakI7QUFDQTtJQUNFLG9CQUFvQjtJQUNwQixjQUFjO0lBQ2QsZUFBZTtFQUNqQjtBQUVBO0lBQ0Usb0JBQW9CO0lBQ3BCLGNBQWM7SUFDZCxlQUFlO0VBQ2pCIiwiZmlsZSI6InNyYy9hcHAvbGl2ZWNoYXQtaG9tZS9jaGF0LXNlbmRlci9jaGF0LXNlbmRlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNoYXRNZXNzYWdlV3JhcHBlcntcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gIC8qICBoZWlnaHQ6IDMwcHg7Ki9cclxuICAgIHdpZHRoOiBjYWxjKDEwMCUgLSA2NXB4KTtcclxuXHRwYWRkaW5nLXRvcDogNXB4O1xyXG59XHJcbi5jaGF0TWVzc2FnZVdyYXBwZXJHcmV5e1xyXG4gIGJhY2tncm91bmQtY29sb3I6IEY3RjdGNztcclxuLyogIGhlaWdodDogMzBweDsqL1xyXG4gIHdpZHRoOiAxMTAlO1xyXG5wYWRkaW5nLXRvcDogNXB4O1xyXG59XHJcbi5jaGF0TWVzc2FnR3JleU91dHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjdGN0Y3O1xyXG5cclxufVxyXG4uYnRuU2VuZE1lc3NhZ2V7XHJcbiAgICBoZWlnaHQ6IDM5cHg7XHJcbiAgICB3aWR0aDogNThweDtcclxuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byBib3R0b20sICNmN2Y3ZjcgMCUsICNmN2Y3ZjcgNTAlLCAjZTdlN2U3IDUwJSwgI2U3ZTdlNyAxMDAlKTtcclxuICAgIGNvbG9yOiAjMDA3M2NmO1xyXG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICAgIGZvbnQtZmFtaWx5OiBBcmlhbDtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDBweDtcclxuICAgIGJvdHRvbTogMjhweDtcclxuICAgIGZsb2F0OiByaWdodDtcclxufVxyXG4uYnRuU2VuZE1lc3NhZ2U6Zm9jdXMge1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxufVxyXG5cclxuLnR4dEFyZWFUeXBlIHtcclxuICBcdHdpZHRoOiAxMDAlO1xyXG4gICAgd2hpdGUtc3BhY2U6IHByZS13cmFwO1xyXG4gICAgb3ZlcmZsb3cteTogYXV0bztcclxuICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgICBsaW5lLWhlaWdodDogMTNweDtcclxuICAgIC13ZWJraXQtbGluZS1icmVhazogYWZ0ZXItd2hpdGUtc3BhY2U7XHJcbiAgICB3b3JkLXdyYXA6IGJyZWFrLXdvcmQ7XHJcbiAgICBvdXRsaW5lLXN0eWxlOiBub25lO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgZm9udC1zaXplOiAxM3B4OyBcclxuICAgIHBhZGRpbmctdG9wOiAxM3B4O1xyXG4gICAgbWFyZ2luOiAwcHg7XHJcbiAgICBmb250LWZhbWlseTogQXJpYWwsIEhlbHZldGljYSxzYW5zLXNlcmlmO1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgcmVzaXplOiBub25lO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAyMHB4O1xyXG4gICAgaGVpZ2h0OiAzOHB4O1xyXG4gIH1cclxuXHJcbiAgLnR4dEFyZWFUeXBlR3JleSB7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICB3aGl0ZS1zcGFjZTogcHJlLXdyYXA7XHJcbiAgICAgIG92ZXJmbG93LXk6IGF1dG87XHJcbiAgICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjojRjdGN0Y3O1xyXG4gICAgICBsaW5lLWhlaWdodDogMTNweDtcclxuICAgICAgLXdlYmtpdC1saW5lLWJyZWFrOiBhZnRlci13aGl0ZS1zcGFjZTtcclxuICAgICAgd29yZC13cmFwOiBicmVhay13b3JkO1xyXG4gICAgICBvdXRsaW5lLXN0eWxlOiBub25lO1xyXG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTNweDsgXHJcbiAgICAgIHBhZGRpbmctdG9wOiAxM3B4O1xyXG4gICAgICBtYXJnaW46IDBweDtcclxuICAgICAgZm9udC1mYW1pbHk6IEFyaWFsLCBIZWx2ZXRpY2Esc2Fucy1zZXJpZjtcclxuICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICByZXNpemU6IG5vbmU7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogMjBweDtcclxuICAgICAgaGVpZ2h0OiAzOHB4O1xyXG4gICAgfVxyXG5cclxuLmNoYXRNZXNzYWdlV3JhcHBlciBidXR0b24ge1xyXG4gICAgd2lkdGg6IDM2cHg7XHJcbiAgICBoZWlnaHQ6IDM2cHg7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgbWFyZ2luOiAwIWltcG9ydGFudDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGJhY2tncm91bmQ6IHVybCgvLi4vLi4vLi4vYXNzZXRzL2ljb25zL2J0bl9zZW5kLnBuZykgbm8tcmVwZWF0IDUwJTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHBhZGRpbmctbGVmdDogODBweDtcclxuICAgLyogcmlnaHQ6IDMwcHg7XHJcbiAgICBib3R0b206IDZweDsqL1xyXG4gICAvKiBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMTgwZGVnLCAjMTU4RkVGIDAlLCAjMDA3M0NGIDEwMCUpOyovXHJcbn1cclxuXHJcbi5jaGF0TWVzc2FnZVdyYXBwZXIgYnV0dG9uOmZvY3VzIHtcclxuICAgIG91dGxpbmU6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxuI21lc3NhZ2VUb1NlbmQ6Oi13ZWJraXQtaW5wdXQtcGxhY2Vob2xkZXIge1xyXG4gICAgZm9udC1mYW1pbHk6IGluaGVyaXQ7XHJcbiAgICBjb2xvcjogIzU4NTg1ODtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICB9XHJcbiAgXHJcbiAgI21lc3NhZ2VUb1NlbmQ6LW1zLWlucHV0LXBsYWNlaG9sZGVyIHtcclxuICAgIGZvbnQtZmFtaWx5OiBpbmhlcml0O1xyXG4gICAgY29sb3I6ICM1ODU4NTg7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgfVxyXG5cclxuICAjbWVzc2FnZVRvU2VuZDo6LW1zLWlucHV0LXBsYWNlaG9sZGVyIHtcclxuICAgIGZvbnQtZmFtaWx5OiBpbmhlcml0O1xyXG4gICAgY29sb3I6ICM1ODU4NTg7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgfVxyXG4gICNtZXNzYWdlVG9TZW5kOi1tb3otcGxhY2Vob2xkZXIge1xyXG4gICAgZm9udC1mYW1pbHk6IGluaGVyaXQ7XHJcbiAgICBjb2xvcjogIzU4NTg1ODtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICB9XHJcbiAgXHJcbiAgI21lc3NhZ2VUb1NlbmQ6Oi1tb3otcGxhY2Vob2xkZXIge1xyXG4gICAgZm9udC1mYW1pbHk6IGluaGVyaXQ7XHJcbiAgICBjb2xvcjogIzU4NTg1ODtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICB9XHJcbiAgXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/livechat-home/chat-sender/chat-sender.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/livechat-home/chat-sender/chat-sender.component.ts ***!
  \********************************************************************/
/*! exports provided: ChatSenderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatSenderComponent", function() { return ChatSenderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_chat_socket_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/chat-socket.service */ "./src/app/services/chat-socket.service.ts");
/* harmony import */ var _services_message_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/message.service */ "./src/app/services/message.service.ts");
/* harmony import */ var _services_context_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/context.service */ "./src/app/services/context.service.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm2015/store.js");
/* harmony import */ var _store_app_selectors__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../store/app.selectors */ "./src/store/app.selectors.ts");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/shared.service */ "./src/app/services/shared.service.ts");








let ChatSenderComponent = class ChatSenderComponent {
    constructor(chatService, msgService, contextService, store, sharedService) {
        this.chatService = chatService;
        this.msgService = msgService;
        this.contextService = contextService;
        this.store = store;
        this.sharedService = sharedService;
        this.messages = [];
        this.enableSendButton = false;
        this.disableMessageBox = false;
        this.context$ = this.store.select(_store_app_selectors__WEBPACK_IMPORTED_MODULE_6__["getContext"]);
        this.sequence = 1;
        this.subscription = this.msgService.getMessage().subscribe(messageList => {
            this.messages = messageList;
        });
        this.subscription1 = this.sharedService.getDisableMessage().subscribe(msgs => {
            this.userMessage = '';
            this.disableMessageBox = true;
            console.log('Disbale UI : ' + this.disableMessageBox);
        });
        this.sharedService.getIsChatEnded().subscribe(chatStatus => {
            console.log('Chat Ended: ' + chatStatus);
            if (chatStatus) {
                // Clear chat message text area before disabling due inactivity
                this.userMessage = '';
                this.disableMessageBox = true;
            }
        });
        this.context$.subscribe(ctx => {
            this.context = ((ctx.liveChatContext === null) ? JSON.parse(sessionStorage.getItem('context')) : ctx);
        });
        this.appProperties = this.context.liveChatContext;
    }
    ngOnInit() {
    }
    sendMessage() {
        console.log('Message Sent');
        this.sequence++;
        // const appProperties: IAppProperties = this.contextService.getChatBotContext();
        if (this.userMessage.trim().length > 0) {
            this.outgoingMsg = {
                message: this.userMessage,
                messageType: 'outgoingMsg',
                userType: 'user',
                timeStamp: this.getDatetime(),
                error: false,
                userName: 'Me'
            };
            // appProperties.userFName + ' ' + appProperties.userLName
            this.msgService.sendMessage(this.outgoingMsg);
            this.sharedService.setSendMessage(this.outgoingMsg);
            const flag = this.chatService.getusermsgpushedflag();
            console.log('this.chatService.getusermsgpushedflag+++' + flag);
            this.chatService.sendMessageLiveChat(this.userMessage, this.sequence, flag, this.appProperties);
            this.chatService.setusermsgpushedflag(true);
        }
        this.userMessage = '';
        this.enableSendButton = false;
    }
    getDatetime() {
        const date = new Date();
        let hours = date.getHours();
        let minutes = date.getMinutes();
        const ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        hours = hours < 10 ? '0' + hours : hours;
        const strTime = hours + ':' + minutes + '' + ampm;
        return strTime;
    }
    onKeyDown($event) {
        if ($event.keyCode === 13) {
            $event.preventDefault();
            this.sendMessage();
        }
        else {
            if (this.userMessage && this.userMessage.trim().length > 0) {
                this.enableSendButton = true;
            }
            else {
                this.enableSendButton = false;
            }
        }
    }
};
ChatSenderComponent.ctorParameters = () => [
    { type: _services_chat_socket_service__WEBPACK_IMPORTED_MODULE_2__["ChatSocketService"] },
    { type: _services_message_service__WEBPACK_IMPORTED_MODULE_3__["MessageService"] },
    { type: _services_context_service__WEBPACK_IMPORTED_MODULE_4__["ContextService"] },
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_5__["Store"] },
    { type: _services_shared_service__WEBPACK_IMPORTED_MODULE_7__["SharedService"] }
];
ChatSenderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-chat-sender',
        template: __webpack_require__(/*! raw-loader!./chat-sender.component.html */ "./node_modules/raw-loader/index.js!./src/app/livechat-home/chat-sender/chat-sender.component.html"),
        styles: [__webpack_require__(/*! ./chat-sender.component.css */ "./src/app/livechat-home/chat-sender/chat-sender.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_chat_socket_service__WEBPACK_IMPORTED_MODULE_2__["ChatSocketService"], _services_message_service__WEBPACK_IMPORTED_MODULE_3__["MessageService"],
        _services_context_service__WEBPACK_IMPORTED_MODULE_4__["ContextService"], _ngrx_store__WEBPACK_IMPORTED_MODULE_5__["Store"],
        _services_shared_service__WEBPACK_IMPORTED_MODULE_7__["SharedService"]])
], ChatSenderComponent);



/***/ }),

/***/ "./src/app/livechat-home/livechat-home.component.css":
/*!***********************************************************!*\
  !*** ./src/app/livechat-home/livechat-home.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".chatHeader{\r\n\tbackground-color: #003087;\r\n\tcolor: #fff;\r\n\tfont-family: Arial, Helvetica,sans-serif;\r\n\tpadding-top: 10px;\r\n\tdisplay: flex;\r\n    align-items: center;\r\n}\r\n\r\n.chat-area {\r\n\t\t/*background-color: #F7F7F7;*/\r\n\t\toverflow-y: auto; \r\n\t\toverflow-x: hidden;\r\n}\r\n\r\n.container{\r\n\tpadding-left: 0px;\r\n\tpadding-right: 0px;\r\n\tposition: fixed;\r\n    bottom: 0px;\r\n}\r\n\r\n.overlay {\r\n  position: fixed;\r\n  top: 0;\r\n  bottom: 0;\r\n  left: 0;\r\n  right: 0;\r\n  background-color: rgba(0, 0, 0, 0.5);\r\n  z-index: 999;\r\n}\r\n\r\n.overlayMsg{\r\n\tdisplay: block;\r\n\tfont-size: 1.5em;\r\n    -webkit-margin-before: 1em;\r\n            margin-block-start: 1em;\r\n    -webkit-margin-after: 1em;\r\n            margin-block-end: 1em;\r\n    -webkit-margin-start: 0px;\r\n            margin-inline-start: 0px;\r\n    -webkit-margin-end: 0px;\r\n            margin-inline-end: 0px;\r\n}\r\n\r\n.overlayCloseBtn{\r\n\tbackground: none !important;\r\n\tborder: none;\r\n\tpadding: 0 !important;\r\n    padding-bottom: 1px !important;\r\n    color: #0561C8;\r\n    font-size: 1.5em;\r\n    margin: 0px;\r\n    margin-bottom: 10px;\r\n    cursor: pointer;\r\n    float: right;\r\n}\r\n\r\n.chatSenderDiv{\r\n    margin-top: auto;\r\n}\r\n\r\n#status{\r\n    align-self: flex-start;\r\n}\r\n\r\n/*IE Edge specific fix - container overflowing the window size*/\r\n\r\n@media screen and (max-width: 372px) {\r\n@supports (-ms-ime-align:auto) and (-webkit-text-stroke:initial) {\r\n        .container{\r\n            width:86% !important;\r\n            min-width: 86% !important;\r\n        }\r\n    }\r\n} \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGl2ZWNoYXQtaG9tZS9saXZlY2hhdC1ob21lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Q0FDQyx5QkFBeUI7Q0FDekIsV0FBVztDQUNYLHdDQUF3QztDQUN4QyxpQkFBaUI7Q0FDakIsYUFBYTtJQUNWLG1CQUFtQjtBQUN2Qjs7QUFFQTtFQUNFLDZCQUE2QjtFQUM3QixnQkFBZ0I7RUFDaEIsa0JBQWtCO0FBQ3BCOztBQUVBO0NBQ0MsaUJBQWlCO0NBQ2pCLGtCQUFrQjtDQUNsQixlQUFlO0lBQ1osV0FBVztBQUNmOztBQUVBO0VBQ0UsZUFBZTtFQUNmLE1BQU07RUFDTixTQUFTO0VBQ1QsT0FBTztFQUNQLFFBQVE7RUFDUixvQ0FBb0M7RUFDcEMsWUFBWTtBQUNkOztBQUNBO0NBQ0MsY0FBYztDQUNkLGdCQUFnQjtJQUNiLDBCQUF1QjtZQUF2Qix1QkFBdUI7SUFDdkIseUJBQXFCO1lBQXJCLHFCQUFxQjtJQUNyQix5QkFBd0I7WUFBeEIsd0JBQXdCO0lBQ3hCLHVCQUFzQjtZQUF0QixzQkFBc0I7QUFDMUI7O0FBQ0E7Q0FDQywyQkFBMkI7Q0FDM0IsWUFBWTtDQUNaLHFCQUFxQjtJQUNsQiw4QkFBOEI7SUFDOUIsY0FBYztJQUNkLGdCQUFnQjtJQUNoQixXQUFXO0lBQ1gsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixZQUFZO0FBQ2hCOztBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksc0JBQXNCO0FBQzFCOztBQUVBLCtEQUErRDs7QUFDL0Q7QUFDQTtRQUNRO1lBQ0ksb0JBQW9CO1lBQ3BCLHlCQUF5QjtRQUM3QjtJQUNKO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9saXZlY2hhdC1ob21lL2xpdmVjaGF0LWhvbWUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jaGF0SGVhZGVye1xyXG5cdGJhY2tncm91bmQtY29sb3I6ICMwMDMwODc7XHJcblx0Y29sb3I6ICNmZmY7XHJcblx0Zm9udC1mYW1pbHk6IEFyaWFsLCBIZWx2ZXRpY2Esc2Fucy1zZXJpZjtcclxuXHRwYWRkaW5nLXRvcDogMTBweDtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG5cclxuLmNoYXQtYXJlYSB7XHJcblx0XHQvKmJhY2tncm91bmQtY29sb3I6ICNGN0Y3Rjc7Ki9cclxuXHRcdG92ZXJmbG93LXk6IGF1dG87IFxyXG5cdFx0b3ZlcmZsb3cteDogaGlkZGVuO1xyXG59XHJcblxyXG4uY29udGFpbmVye1xyXG5cdHBhZGRpbmctbGVmdDogMHB4O1xyXG5cdHBhZGRpbmctcmlnaHQ6IDBweDtcclxuXHRwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICBib3R0b206IDBweDtcclxufVxyXG5cclxuLm92ZXJsYXkge1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICB0b3A6IDA7XHJcbiAgYm90dG9tOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgcmlnaHQ6IDA7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjUpO1xyXG4gIHotaW5kZXg6IDk5OTtcclxufVxyXG4ub3ZlcmxheU1zZ3tcclxuXHRkaXNwbGF5OiBibG9jaztcclxuXHRmb250LXNpemU6IDEuNWVtO1xyXG4gICAgbWFyZ2luLWJsb2NrLXN0YXJ0OiAxZW07XHJcbiAgICBtYXJnaW4tYmxvY2stZW5kOiAxZW07XHJcbiAgICBtYXJnaW4taW5saW5lLXN0YXJ0OiAwcHg7XHJcbiAgICBtYXJnaW4taW5saW5lLWVuZDogMHB4O1xyXG59XHJcbi5vdmVybGF5Q2xvc2VCdG57XHJcblx0YmFja2dyb3VuZDogbm9uZSAhaW1wb3J0YW50O1xyXG5cdGJvcmRlcjogbm9uZTtcclxuXHRwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMXB4ICFpbXBvcnRhbnQ7XHJcbiAgICBjb2xvcjogIzA1NjFDODtcclxuICAgIGZvbnQtc2l6ZTogMS41ZW07XHJcbiAgICBtYXJnaW46IDBweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbn1cclxuXHJcbi5jaGF0U2VuZGVyRGl2e1xyXG4gICAgbWFyZ2luLXRvcDogYXV0bztcclxufVxyXG5cclxuI3N0YXR1c3tcclxuICAgIGFsaWduLXNlbGY6IGZsZXgtc3RhcnQ7XHJcbn1cclxuXHJcbi8qSUUgRWRnZSBzcGVjaWZpYyBmaXggLSBjb250YWluZXIgb3ZlcmZsb3dpbmcgdGhlIHdpbmRvdyBzaXplKi9cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogMzcycHgpIHtcclxuQHN1cHBvcnRzICgtbXMtaW1lLWFsaWduOmF1dG8pIGFuZCAoLXdlYmtpdC10ZXh0LXN0cm9rZTppbml0aWFsKSB7XHJcbiAgICAgICAgLmNvbnRhaW5lcntcclxuICAgICAgICAgICAgd2lkdGg6ODYlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIG1pbi13aWR0aDogODYlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59ICJdfQ== */"

/***/ }),

/***/ "./src/app/livechat-home/livechat-home.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/livechat-home/livechat-home.component.ts ***!
  \**********************************************************/
/*! exports provided: LivechatHomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LivechatHomeComponent", function() { return LivechatHomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_chat_socket_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/chat-socket.service */ "./src/app/services/chat-socket.service.ts");
/* harmony import */ var _services_context_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/context.service */ "./src/app/services/context.service.ts");
/* harmony import */ var _services_message_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/message.service */ "./src/app/services/message.service.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm2015/store.js");
/* harmony import */ var _store_app_selectors__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../store/app.selectors */ "./src/store/app.selectors.ts");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/shared.service */ "./src/app/services/shared.service.ts");










let LivechatHomeComponent = class LivechatHomeComponent {
    constructor(messageService, route, chatService, contextService, store, sharedService, renderer, el) {
        this.messageService = messageService;
        this.route = route;
        this.chatService = chatService;
        this.contextService = contextService;
        this.store = store;
        this.sharedService = sharedService;
        this.renderer = renderer;
        this.el = el;
        this.title = 'livechat';
        this.isChrome = false;
        this.scrollHeight = 0;
        this.showDialog = false;
        this.message = '';
        this.isDisable = false;
        this.context$ = this.store.select(_store_app_selectors__WEBPACK_IMPORTED_MODULE_7__["getContext"]);
        // const appProperties: IAppProperties = this.contextService.getChatBotContext();
        this.context$.subscribe(ctx => {
            this.context = ((ctx.liveChatContext === null) ? JSON.parse(sessionStorage.getItem('context')) : ctx);
        });
        const appProperties = this.context.liveChatContext;
        this.quoteNumber = appProperties.quote_number;
        // detect browser for sound IE issue fix
        // this.isChrome = !!window.chrome && !!window.chrome.webstore;
        console.log('App Properties ' + appProperties);
        if (appProperties !== undefined) {
            this.chatService.initSocketConnection(appProperties);
        }
        this.sharedService.getModalParams().subscribe(messageObj => {
            console.log('messageObj.flag ' + messageObj.flag);
            this.showDialog = messageObj.flag;
            if (this.showDialog) {
                this.isDisable = true;
            }
            this.message = messageObj.message;
        });
    }
    ngOnInit() {
    }
    ngAfterViewChecked() {
        this.livechatHeaderHeight = this.liveChatHeader.nativeElement.offsetHeight;
        const part = this.el.nativeElement.querySelector('.chatHeader');
        this.renderer.setElementStyle(part, 'max-height', (this.livechatHeaderHeight + 'px'));
        this.livechatSenderHeight = this.liveChatSender.nativeElement.offsetHeight;
        const part1 = this.el.nativeElement.querySelector('.chatSenderDiv');
        this.renderer.setElementStyle(part1, 'max-height', ('48px'));
        this.livechatMsgAreaHeight = this.liveChatMsgDisplayArea.nativeElement.offsetHeight;
        this.scrollHeight = this.livechatMsgAreaHeight;
        if (this.scrollHeight < this.liveChatMsgDisplayArea.nativeElement.scrollHeight) {
            this.liveChatMsgDisplayArea.nativeElement.scrollTop = this.liveChatMsgDisplayArea.nativeElement.scrollHeight;
            this.scrollHeight = this.liveChatMsgDisplayArea.nativeElement.scrollHeight;
        }
    }
    beforeunloadHandler(event) {
        this.chatService.endLiveChat(this.quoteNumber);
    }
};
LivechatHomeComponent.ctorParameters = () => [
    { type: _services_message_service__WEBPACK_IMPORTED_MODULE_5__["MessageService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_chat_socket_service__WEBPACK_IMPORTED_MODULE_3__["ChatSocketService"] },
    { type: _services_context_service__WEBPACK_IMPORTED_MODULE_4__["ContextService"] },
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_6__["Store"] },
    { type: _services_shared_service__WEBPACK_IMPORTED_MODULE_8__["SharedService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('liveChatContainer', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], LivechatHomeComponent.prototype, "liveChatContainer", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('liveChatMsgDisplayArea', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], LivechatHomeComponent.prototype, "liveChatMsgDisplayArea", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('liveChatHeader', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], LivechatHomeComponent.prototype, "liveChatHeader", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('liveChatSender', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], LivechatHomeComponent.prototype, "liveChatSender", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:beforeunload', ['$event']),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
], LivechatHomeComponent.prototype, "beforeunloadHandler", null);
LivechatHomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-livechat-home',
        template: __webpack_require__(/*! raw-loader!./livechat-home.component.html */ "./node_modules/raw-loader/index.js!./src/app/livechat-home/livechat-home.component.html"),
        styles: [__webpack_require__(/*! ./livechat-home.component.css */ "./src/app/livechat-home/livechat-home.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_message_service__WEBPACK_IMPORTED_MODULE_5__["MessageService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
        _services_chat_socket_service__WEBPACK_IMPORTED_MODULE_3__["ChatSocketService"], _services_context_service__WEBPACK_IMPORTED_MODULE_4__["ContextService"],
        _ngrx_store__WEBPACK_IMPORTED_MODULE_6__["Store"], _services_shared_service__WEBPACK_IMPORTED_MODULE_8__["SharedService"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]])
], LivechatHomeComponent);



/***/ }),

/***/ "./src/app/services/context.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/context.service.ts ***!
  \*********************************************/
/*! exports provided: ContextService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContextService", function() { return ContextService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ContextService = class ContextService {
    constructor() { }
    getAppName() {
        return this.appName;
    }
    setAppName(appName) {
        this.appName = appName;
    }
    setChatBotContext(chatBotContext) {
        this.chatBotContext = chatBotContext;
    }
    getChatBotContext() {
        return this.chatBotContext;
    }
};
ContextService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], ContextService);



/***/ })

}]);
//# sourceMappingURL=ffq-livechat-module-es2015.js.map