const socketIO = require('socket.io');
const _ = require('underscore');
const logger = require('./utility/logger');
const watsonService = require('./libraries/watson_library.js');
const api = require('./libraries/api_ques_ans.js');
var fs = require("fs");
const REDIS_URL = process.env.REDIS_URL;
const REDIS_KEY = process.env.REDIS_PASSWORD || 'chat-socket';
var dataModels = require('./modules/common/data_models_provider_service');
let io = {};
let users = [];

module.exports = {
    //socket.io server creation
    createSocket: function (server) {
        io = socketIO(server);
        // io.set('transports', ['websocket','polling']);
        //io.adapter(redisAdapter);
        logger.info(__filename, 'Socket io server created');
    },

    socketConnection: function () {
        io.use(function (socket, next) {
            next();
        }).on('connection', function (socket) {

                logger.info('User connected');

                socket.on('welcome message', function (msg) {

                    logger.info('welcome message event caught at orchestration layer BOTTTT');
                    logger.info('Live Chat welcome message event caught at orchestration layer');
                    let liveChatWelcomeMsg = dataModels.constantMessages.liveChatWelcomeMessage;
                    const data = dataModels.chatResponse;
                    data.message = liveChatWelcomeMsg;
                    watsonService.createSessionId(function(session_id) {
                        data.sessionID = session_id;
                        socket.emit('LiveChatWelcomeMessageUpdateToScreen', data);
                    });
                    // watsonService.createSessionId(data, socket);
                    // let liveChatWelcomeHelpMsg = { 'msg': dataModels.constantMessages.liveChatWelcomeHelpMsg, 'msgType': "welcomeMessage", 'error': false };
                    // let liveChatWelcomeDisclaimerMsg = { 'msg': dataModels.constantMessages.liveChatWelcomeDisclaimerMsg, 'msgType': "welcomeMessage", 'error': false };

                });

                socket.on('send message', function (data, intentFlag) {
                    console.log('intentFlag', data.intentFulfilled);
                    if (data.intentFulfilled == 'true') {
                        api.callApi(data.msg, function (err, response) {
                            console.log(response.score);
                            if (err == null && response != null && response.score > 85) {
                                const chatResponse = dataModels.chatResponse;
                                chatResponse.message = response.answer.replace(/(?:\\[r]|[\r]+)+/g, "\\r");
                                chatResponse.error = false;
                                chatResponse.intentFulfilled = true;
                                if (socket) {
                                    console.log("Going to emit new message", chatResponse)
                                    socket.emit('new message', chatResponse);
                                }
                            } else {
                                watsonService.sendWatsonRequest(data, false, null, socket);
                            }
                        })
                    }
                    else {
                        watsonService.sendWatsonRequest(data, false, null, socket);
                    }

                })


            });


    }
};