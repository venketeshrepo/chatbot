'use strict';

const settings 		= require('../project_settings.js');
const currentEnv 	= settings.ENVIRONMENT;

const configMap = {

	'development' : {
		'token' : '430134c9-9824-4d34-8b1a-3f55d29c6785'
	},
	'prod' : {
		'token' : ''	
	},
	'aft' : {
		'token' : '3efa29f3-c2ea-4e80-870d-918310287e6a'
	},
	'per' : {
		'token' : '3efa29f3-c2ea-4e80-870d-918310287e6a'
	},
	'reg' : {
		'token' : '3efa29f3-c2ea-4e80-870d-918310287e6a'
	}
};

module.exports.token = process.env.LOG_ENTRIES_TOKEN || configMap[currentEnv].token;