/******************************************************************************
* @Filename : logger.js
* @ProjectName : FFQ Chatbot
* @Author : Treesa Mary N J
* @CreatedOn : 2017-OCT-17
* @Description : Node module for handling logging 
*****************************************************************************/

'use strict';

const bunyan 			= require('bunyan');
const settings 			= require('../project_settings.js');
const config 			= require('../config/logentries_config.js');
const logLevel 			= (settings.LOGGER.LOGGER_LEVEL) ? settings.LOGGER.LOGGER_LEVEL : 'WARN';
const currentEnv 		= settings.ENVIRONMENT;
const logTrail 			= require('le_node');
const loggerDefinition 	= logTrail.bunyanStream({ token: config.token });


let logger = bunyan.createLogger({
	src:true,
	name: currentEnv,
	streams: [
		loggerDefinition,
		{
			name: 'console',
			stream: process.stdout,
			level: logLevel
		}
	]
});
logger.level(logLevel);

module.exports = logger;