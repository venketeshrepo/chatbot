/******************************************************************************
* @Filename : common.js
* @ProjectName : FFQ Chatbot
* @Author : Treesa Mary N J
* @CreatedOn : 2017-DEC-07
* @Description : Node module for handling common utility functions 
*****************************************************************************/

'use strict';

const momentTime = require('moment-timezone');

function getPSTDate() {
    const date = new Date();
    const timestamp = date.getTime();
    return momentTime.tz(timestamp, 'America/Los_Angeles').format();
}

function isTestData(chatDetails) {
    const testUser = /\bTEST\b/i; 
    var isTestUser = false;
    if(chatDetails.userName.toUpperCase().match(testUser)) {
        isTestUser = true;
    }
    return isTestUser;
}

module.exports.getPSTDate = getPSTDate;
module.exports.isTestData = isTestData;
