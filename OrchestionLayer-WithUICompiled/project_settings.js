/******************************************************************************
* @Filename : project_settings.js
* @ProjectName : FFQ Chatbot
* @Author : Treesa Mary N J
* @CreatedOn : 2017-OCT-17
* @Description : File for configuring project settings
*****************************************************************************/

module.exports = {
	'PROJECT_DIR' : __dirname,
	'LOGGER' :{
		'LOGGER_LEVEL' : process.env.LOG_LEVEL || 'INFO'
	},
	'SERVER':{
		'PORT': 3000,
		'ADDRESS': 'localhost'
	},
	'ENVIRONMENT' : process.env.ENVIRONMENT || 'development',
	'ISDBINSERT' : process.env.ISDBINSERT || 'true'
};