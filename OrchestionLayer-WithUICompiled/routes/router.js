'use strict';

var fs = require('fs');
const express = require('express');
var request = require('request');
const router = express.Router();
// const jwtAuth = require('../modules/jwtauth/jwtauth.js');
const logger = require('../utility/logger');
const commonUtil = require('../utility/common.js');
const settings 		= require('../project_settings.js');
const serverPort 	= parseInt(process.env.PORT, 10) || settings.SERVER.PORT;

/* GET home page. */
router.get('/', function(req, res) {
  res.render('main');
});


router.get('/post', function(req, res) {
  console.log('process.env.CLOUD_DEPLOYED   ->>>>>', process.env.CLOUD_DEPLOYED);
  if (process.env.CLOUD_DEPLOYED === 'Y') {
    console.log('PORT   ->', serverPort);
    res.render('index-chat', {appName : 'chatbot', orchUrl : 'https://'+req.hostname });
  } else {
    res.render('index-chat', {appName : 'chatbot', orchUrl : 'http://'+req.hostname+':'+serverPort });
  }
  
});


function getIpAddress(req){ 
    let remoteAddress = req.connection.remoteAddress.toString();
    let ip = remoteAddress.slice(remoteAddress.lastIndexOf(':') +1,remoteAddress.length);
    return ip;
}

module.exports = router;