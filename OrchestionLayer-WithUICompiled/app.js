'use strict';
const cors = require('cors');
const express = require('express');
const helmet = require('helmet');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const routes = require('./routes/router');
const logger = require('./utility/logger');

const app = express();
app.disable('x-powered-by');
app.disable('X-Powered-By');

app.engine('html', require('ejs').renderFile);
app.use(express.static(__dirname + '/dist/ffq-chat-ui'));

// view engine setup
app.set('views',  path.join(__dirname, '/dist/ffq-chat-ui') );
// app.set('view engine', 'jade');
app.set('view engine', 'html');


app.use(helmet({
  frameguard: false
})); 
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

/** catch 404 and forward to error handler */
app.use(function(req, res) {
	res.status(200).json({});
});

app.use(function(err, req, res) {
    logger.error('Message : ', err.message);
    res.status(err.status || 500).json({
        message: err.message
    });
});


module.exports = app;