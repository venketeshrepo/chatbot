'use strict';
/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */
exports.config = {
  /**
   * Array of application names.
   */
  app_name: ['Covid Global Assistant'],
  /**
   * Your New Relic license key.
   */
  license_key: 'bced6c5e4f205607fdc075bf66d98b2492c9df71',
  logging: {
    /**
     * Level at which to log. 'trace' is most useful to New Relic when diagnosing
     * issues with the agent, 'info' and higher will impose the least overhead on
     * production applications.
     */
    level: 'info'
  },
  rules : {
    ignore : [ '^/socket.io' ]
  }
};
