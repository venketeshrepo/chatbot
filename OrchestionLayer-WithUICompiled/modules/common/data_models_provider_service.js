
'use strict';

var jsonFile = require('jsonfile');
var path = require('path');


var chatResponse = jsonFile.readFileSync(path.join(__dirname, '../../data_models/dynamic/chatResponse.json'));
var constantMessages = jsonFile.readFileSync(path.join(__dirname, '../../data_models/constants/messages_constants.json'));


module.exports.chatResponse = Object.assign(chatResponse);
module.exports.constantMessages = Object.assign(constantMessages);
