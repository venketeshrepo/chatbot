'use strict';
var request = require('request');

module.exports = {

  callApi: function (msg, callback) {
    const options = {
      url: 'https://ibmchallengecov19.azurewebsites.net/qnamaker/knowledgebases/62a7a195-ed38-4530-b1d5-f7315867db21/generateAnswer',
      method: 'post',
      body: {
        question: msg
      },
      json: true,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'EndpointKey 28801a13-e92d-407c-9eed-fb639b82db7a'
      }
    }
    request(options, function (error, response, body) {

      if (!error && response.statusCode === 200) {
        console.log('Api response', body.answers[0]);
        callback(null, body.answers[0]);

      }
      else {
        callback(error, null)
      }

    });
  }
};