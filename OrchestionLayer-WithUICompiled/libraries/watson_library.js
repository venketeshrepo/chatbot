'use strict';

const logger = require('../utility/logger.js')
const AssistantV2 = require('ibm-watson/assistant/v2');
const { IamAuthenticator } = require('ibm-watson/auth');
var dataModels = require('../modules/common/data_models_provider_service');

const assistant = new AssistantV2({
	version: '2020-04-01',
	authenticator: new IamAuthenticator({
		apikey: '20DRF1KWwievRcmRUGAf7oBRPR0IYAuxICcvjNiY_QbU',
	}),
	url: 'https://api.eu-gb.assistant.watson.cloud.ibm.com/instances/8457f9cc-3518-4761-a7b9-694cf6df9822'
});

module.exports = {

	sendWatsonRequest: function (data, isFromSuggestion, selectedIntent, socket) {

		/* response message sent back to chat window */
		var chatResponse = dataModels.chatResponse;
		console.log('\n\nRequest from UI--->' + JSON.stringify(data));
		var msg = data.msg;
		if (msg !== null && msg !== 'undefined') {
			console.log("\n\nMessage from user : " + msg);
			msg = msg.replace(/\s\s+/g, ' ').replace('.', ' ').replace(/,/g, "").replace(/['"]+/g, '').replace(/\<br\/>/g, '');
			var lengthToTrim = msg.length - 1;
			if (msg.startsWith("\"") && msg.endsWith("\"")) {
				lengthToTrim = msg.length - 2;
				msg = msg.substr(1, lengthToTrim);
			}
			msg = msg.trim();
		}
		var sess_iD = data.sessionID;

		var watsonRequest = {
			'message_type': 'text',
			'text': msg,
			'options': {
				'return_context': true
			}
		};

		if (isFromSuggestion) {
			watsonRequest['intents'] = selectedIntent;
		}
		console.log("\n\n Input send to watson : " + JSON.stringify(watsonRequest));
		assistant.message({
			assistantId: '15010143-7fe6-4a6c-bbe3-7d1b716e08de',
			sessionId: sess_iD,
			input: watsonRequest,

		})
			.then(res => {
				console.log('\n\nResponse From Watson---> ' + JSON.stringify(res.result, null, 2));
				if (res.result.output.generic[0].response_type === "text") {
					chatResponse.message = res.result.output.generic[0].text;
					chatResponse.sessionID = sess_iD;
					var requiredNewSession = !(res.result.context.skills["main skill"].user_defined === undefined);
					this.emitChatResponse(chatResponse, requiredNewSession, socket);
				} else if (res.result.output.generic[0].response_type === "suggestion") {

					const suggestions = res.result.output.generic[0].suggestions;
					const confidence = suggestions[0].value.input.intents[0].confidence;
					if (parseFloat(confidence).toFixed(2) > 0.75) {
							this.sendWatsonRequest(data, true, suggestions[0].value.input.intents, socket);
					} else {
						chatResponse.message = dataModels.constantMessages.rephraseMessage;
						this.emitChatResponse(chatResponse, true, socket);
					}
				} else {
					chatResponse.message = dataModels.constantMessages.rephraseMessage;
					this.emitChatResponse(chatResponse, true, socket);
				}
			})
			.catch(err => {
				console.log("\n\n Error Response From Watson : " + JSON.stringify(err));
				chatResponse.message = "";
				if (err.message === 'Invalid Session') {
					chatResponse.message = dataModels.constantMessages.sessionExpiredMessage;
				}
				chatResponse.sessionID = sess_iD;
				chatResponse.error = true;
				console.log("\n\n Emitting the error message : " + JSON.stringify(chatResponse)); 
				socket.emit('new message', chatResponse);
			});


	},

	createSessionId: function (callback) {
		var sess_id = '';
		assistant.createSession({
			assistantId: '15010143-7fe6-4a6c-bbe3-7d1b716e08de'
		})
			.then(res => {
				console.log(JSON.stringify(res.result, null, 2));
				sess_id = res.result.session_id;
				console.log(sess_id);
				callback(sess_id);
			})
			.catch(err => {
				console.log(err);
				callback(sess_id);
			});
	},

	emitChatResponse: function (chatResponse, requiredNewSession, socket) {
		chatResponse.error = false;
		if (!requiredNewSession) {
			chatResponse.intentFulfilled = false;
			socket.emit('new message', chatResponse);
		}
		else {
			this.createSessionId(function (session_id) {
				chatResponse.intentFulfilled = true;
				chatResponse.sessionID = session_id;
				socket.emit('new message', chatResponse);
			});

		}

	}

};